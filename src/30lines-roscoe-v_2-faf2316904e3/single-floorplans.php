<?php
global $post, $rentPress_Service;

$FloorplanMeta=$rentPress_Service['floorplans_meta']->setPostID($post->ID);

$PropertyOfFloorplan = get_posts([
	'fields' => "ids",
	'post_type' => 'properties',
	'property_code' => get_post_meta($post->ID, 'parent_property_code', true),
	'posts_per_page' => 1,
])[0];

$PropertyAmenities = json_decode(get_post_meta($PropertyOfFloorplan, 'amenities', true));

$background = wp_get_attachment_image_src( get_post_thumbnail_id( $PropertyOfFloorplan ), 'full' );

$FloorplanBeds=$FloorplanMeta->beds();

get_header(); ?>

	<header class="hero is-standard is-minimal has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
		
		<section class="hero-content-wrapper animated fadeIn row">
			
		</section>

	</header>
	
<?php while ( have_posts() ) : the_post();
	$FloorplanMeta = $rentPress_Service['floorplans_meta']->setPostID($post->ID); 
	$minSQFT = $FloorplanMeta->sqftg('min');
	$maxSQFT = $FloorplanMeta->sqftg('max'); ?>
<div id="single-fp-inner-angular">
	<main id="main" class="" role="main">
		<section class="floorplan-overview row is-narrow medium-collapse" data-equalizer>

			<div class="medium-6 columns" data-equalizer-watch>
				<figure>
					<img src="<?php echo $FloorplanMeta->image(); ?>" alt="">
				</figure>
			</div>

			<aside class="medium-6 columns true-center" data-equalizer-watch>

				<div class="overview-wrapper">
					<header class="fp-details">
						<h2 class=""><?php the_title(); ?></h2>
						<h3><?php echo (($FloorplanBeds == 0)?"Studio":$FloorplanBeds.' Bed'); ?> <span class="accent-text bold">|</span> <?php echo number_format($FloorplanMeta->baths(), 0); ?> Bath</h3>
						<h5>Starting at $<?php echo $FloorplanMeta->rent(); ?></h5>
						<h5>
							<?php if ( intval($minSQFT) === intval($maxSQFT) ) :  ?>
								<?php echo $FloorplanMeta->sqftg('min'); ?> Sq. Ft. 
								<span class="accent-text bold">|</span> <?php echo $FloorplanMeta->availableUnitCount(); ?> Available Now
							<?php else : ?>
								<?php echo $FloorplanMeta->sqftg('min').' - '.$FloorplanMeta->sqftg('max'); ?> Sq. Ft. 
								<span class="accent-text bold">|</span> <?php echo $FloorplanMeta->availableUnitCount(); ?> Available Now
							<?php endif; ?>
						</h5>
					</header>
					<div class="fp-highlights">
						<h6>Floor Plan Highlights</h6>
						<ul>
							<?php 
							foreach ( $PropertyAmenities as $amenity ) :
								if ( preg_match('/Custom Amenity/', $amenity->Title) ) continue; 
								echo '<li>'. $amenity->Title .'</li>';
							endforeach; ?>
						</ul>
						<a href="#" class="button alt-btn" data-open="requestInfoModal">Request Info</a>
						<a 
							href="/tour?fp_name=<?php echo urlencode(esc_html($post->fpName)).'&bed_count='.esc_attr($post->fpBeds); ?>" 
							class="button alt-ghost-btn"
						>Schedule Tour</a>
					</div>
					
				</div>

			</aside>

		</section>

		<rent-press-units fp-id="<?php echo get_post_meta($post->ID, 'fpID', true); ?>">
			<section class="floorplan-data row is-narrow">
				<div class="floor-plan-unit-table-wrap">
					<!-- 	
						<div ng-if="gathering_from_api" class="units-load-indicator">
							<span style="display:inline-block;vertical-align: middle;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></span> 
							<span style="display:inline-block;vertical-align: middle;">Checking for available apartments...</span>
						</div>
 					-->
					
					<div ng-if="units.length == 0" class="no-units-response">
						<h2>We're sorry!</h2>
						<div class="no-units-response-message">
							<b>There are currently no available apartments with this floor plan.</b> 
							Please <a href="<?php echo get_permalink($PropertyOfFloorplan); ?>">visit the main property page</a> to review other floor plans, or <a href="/communities">visit the advanced search page</a> to browse our full property listing.
						</div>
					</div>

					<header class="form-fp-select small-12 columns" ng-show="units.length">
						<form action="" class="" ng-init="pricingViewer='';">
							<select ng-model="pricingViewer" ng-show="values_of_filters_from_units.termRent.length > 0">
								<option value="">Choose Lease Term</option>

								<option ng-repeat="mt in values_of_filters_from_units.termRent" value="{{mt}}">{{ mt }} Months</option>
							</select>

							<input type="date" ng-model="flashFilters.move_in_date" ng-value="'<?php echo date('Y-m-d'); ?>'">
					
							{{ filter_units.move_in_date }}
						</form>
					</header>

					<table ng-if="units.length" class="stack" ts-wrapper>
						<thead>
							<tr>
								<th ts-criteria="Information.Name">Unit</th>
								<th ts-criteria="rent" ts-default>Price / Mo</th>
								<th ts-criteria="SquareFeet.Max">SQ. FT.</th>
								<th>Availablity</th>
								<th>Action</th>
							</tr>
						</thead>

						<tbody>
							<tr ng-repeat="unit in units | flashFilterUnits:flashFilters" ts-repeat="">
								<td>{{ unit.Information.Name }}</td>
								<td>${{ unit | displayUnitPrice:pricingViewer }}</td>
								<td>{{ unit.SquareFeet.Max }}</td>
								<td>{{ (unit.Information.isAvailable)?"Available Now":"Available "+unit.Information.AvailableOn }}</td>
					      		<td>
					      			<a href="#" class="button">Request Info</a>
					      			<a href="{{ unit.Information.AvailabilityURL }}" class="button alt-btn">Apply Now <i class="fa fa-angle-double-right"></i></a>
					      		</td>
							</tr>
						</tbody>
					</table>
				</div>
			</section>
		</rent-press-units>

		<rent-press-property-floorplans 
			current-floorplan-id='<?php the_ID(); ?>'
			prop-code="<?php echo get_post_meta($post->ID, 'parent_property_code', true); ?>" 
			init-query-args='{"orderby": "title", "post__not_in": [<?php echo $post->ID; ?>], "floorplans_of_similar": true, "floorplans_beds": <?php echo $FloorplanBeds; ?>, "floorplans_baths": <?php echo $FloorplanMeta->baths(); ?>, "posts_per_page": 3, "floorplans_with_available_units": true}'>
			<section ng-if="floorplans.length" class="more-floorplans padded-y">
			
				<div class="row is-narrow" ng-hide="filteredAndOrderedFloorplans.length == 0">
					<div class="small-12 columns">
						<h3>Similar Floor Plans</h3>
					
						<section class="floorplan-grid clearfix">

							<div class="is-floorplan" ng-repeat="floorplan in floorplans | flashFilterFloorplans:flashFilters | limitTo:3">
								<figure class="is-photo">
									<a href="{{ floorplan.permalink }}">
										<img ng-src="{{ floorplan.image }}" alt="">
									</a>
								</figure>
								<section class="floorplan-data">
									<h4>{{ floorplan._title }}</h4>

									<!-- Show if rent is more than 0 -->
									<h6 ng-if="floorplan.meta_data.fpMinRent !== -1">
										From ${{ floorplan.meta_data.fpMinRent }}/MO.
									</h6>

									<!-- Show if rent is less than or equal to 0 -->
									<h6 ng-if="floorplan.meta_data.fpMinRent === -1">
										Call for Pricing
									</h6>

									<p class="is-count">
										<span>{{ floorplan.meta_data.fpBeds | text_for_beds:false:false }} </span> | 
										<span>{{ floorplan.meta_data.fpBaths | text_for_baths:false:false }}</span> | 
										<span>{{ floorplan.meta_data.fpMinSQFT }} Sq. Ft.</span> 
									</p>

									<p class="fp-avail-count" ng-if="floorplan.from_available_units.numOfAvailable != 0">
										{{ floorplan.from_available_units.numOfAvailable }} Available Now
									</p>

									<p class="fp-avail-count" ng-if="floorplan.from_available_units.numOfAvailable == 0">
										No Apartments Available
									</p>

									<a href="{{ floorplan.permalink }}" class="button">View Details</a>
								</section>
							</div> <!-- is-floorplan -->	
							
						</section>
					</div>
				</div>

			</section>

			<footer class="text-center">
				<a href="<?php echo get_permalink($PropertyOfFloorplan); ?>" class="button">Back To All Floor Plans</a>
			</footer>
		</rent-press-property-floorplans>
	</main><!-- #main -->
</div>
<?php endwhile; // End of the loop. ?>

<?php get_template_part( 'template-parts/content', 'cta-banner' ); ?>

<script type="text/javascript">
	// init rentpress angular for map
	var rentPress_App = angular.bootstrap(
		document.getElementById('angular-rent-press-property-results-as-gmap'), 
		['rentPress']
	);	

	var rentPress_TemplateApp = angular.bootstrap(
		document.getElementById('single-fp-inner-angular'), 
		['rentPress']
	);

</script>

<div class="reveal" id="requestInfoModal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
	
	<?php echo do_shortcode('[gravityform id="1"]'); ?>

	<button class="close-button" data-close aria-label="Close modal" type="button">
	<span aria-hidden="true">&times;</span>
	</button>
</div>

<?php
get_footer();
