 <?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 30_Lines_Properties
 */

?>

	</div><!-- #content -->

	<footer class="main-footer" role="contentinfo">
		<div class="row footer-info" data-equalizer>

			<div class="large-7 columns is-info" data-equalizer-watch>
				
				<h5>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></h5>

				<?php
					$footer = array(
						'theme_location' => 'footer', 
						'menu_id' => 'footer-menu',
						'menu_class' => 'footer-menu',
						'container'=>false
					);
					wp_nav_menu( $footer );
				?>

				<?php if ( is_singular('properties') ) : ?>
					<div class="icon-credit" style="font-size: 0.2rem;margin-top: 1rem;">
						Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> 
						from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> 
						is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
					</div>
				<?php endif; ?>

			</div>

			<aside class="large-5 columns is-social" data-equalizer-watch>
				<ul style="list-style-type:none;margin:0.3rem 1rem;">
					<li 
						style="
							background: url(/wp-content/uploads/2017/10/equal-housing.png);
						    min-height: 3rem;
						    width: 3rem;
						    background-repeat: no-repeat;
						    background-size: contain;
					    "
					></li>
				</ul>
				<?php
						$footer = array(
							'theme_location' => 'social_links', 
							'menu_id' => 'social-links',
							'menu_class' => 'social-links',
							'container'=>false
						);
						// if ( has_nav_menu( $footer ) ) {
							wp_nav_menu( $footer );
						// }
					?>
			</aside>

		</div><!-- .footer-info -->

	</footer><!-- main-footer -->

<?php wp_footer(); ?>

</body>
</html>
