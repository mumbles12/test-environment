<?php
/**
 * Property template for a single property display
 */
global $rentPress_Service;

get_header(); ?>
	<div id="angular-tp-single-prop-wrapper">
			
		<?php while ( have_posts() ) : the_post(); 
			$postID       = $post->ID;
			$propertyService = $rentPress_Service['properties_meta']->setPostID($postID);
			$description  = $post->propDescription;
			$background   = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>

			<header class="hero is-single-prop has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
				<section class="hero-content-wrapper">
					<h1 class="animated fadeInUp"><?php the_title(); ?></h1>
					<a href="https://maps.google.com/?saddr=My%20Location&daddr=<?php echo urlencode($propertyService->address(null, true)); ?>" target="_blank">
						<span>
							<i class="fa fa-map-marker"></i> <?php echo $post->propAddress.', '.$post->propCity.', '.$post->propState.' '.$post->propZip; ?>
						</span>
					</a>
					<span class="quick-links">
						<a href="tel:<?php echo $post->propPhoneNumber; ?>"><i class="fa fa-mobile"></i> <?php echo $post->propPhoneNumber; ?></a>
						<a  
							href="https://maps.google.com/?saddr=My%20Location&daddr=<?php echo urlencode($propertyService->address(null, true)); ?>" 
						    target="_blank"
						>
							<i class="fa fa-map-marker"></i> Get Directions
						</a>
						<a 
							href="<?php echo esc_url($post->propURL); ?>" 
							target="_blank"
						><i class="fa fa-laptop"></i> Property Website</a>
					</span>
					<footer class="is-cta-links">
						<a href="/contact" class="button">Contact Leasing</a>
						<a href="<?php echo esc_url($post->propAvailUrl); ?>" class="button alt-btn" target="_blank">Apply Online</a>
					</footer>
				</section>
			</header>

			<?php if( strtotime(get_field('start_date')) <= strtotime(date('Y-m-d')) && strtotime(get_field('end_date')) >= strtotime(date('Y-m-d')) ) : ?>
				<div class="special-banner">
					<h4><?= the_field('title_of_special'); ?></h4>
					<span><?= the_field('description_of_special'); ?></span>
				</div>
			<?php endif; ?>

			<main id="main" role="main">
				<section class="prop-main-details" ng-controller="mainCtrl">
					<div class="clearfix small-collapse">

						<div class="medium-4 large-3 columns">
							<ul class="vertical tabs" data-tabs id="example-tabs">
								<li class="tabs-title is-active"><a href="#overview" aria-selected="true">Overview</a></li>
								<li class="tabs-title"><a href="#panel1v">Floor Plans</a></li>
								<li class="tabs-title"><a href="#panel2v">Amenities</a></li>
								<?php 
								$hoodPostID = get_post_meta($post->ID, 'post_type_connected_to_neighborhoods', true); 
								if ( $hoodPostID ) :  ?>
									<li class="tabs-title"><a href="#panel3v">Neighborhood</a></li>
								<?php endif; ?>
								<li class="not-tabs-title"><a href="#gallery" class="smooth-scroll">Gallery</a></li>
							</ul>
						</div>

						<div class="medium-8 large-9 columns">
							<div class="tabs-content" data-tabs-content="example-tabs">
								<div class="tabs-panel is-active is-overview" id="overview">

									<h3><?php the_title(); ?></h3>

									<ul class="prop-amenities featured">
										<?php 
											echo apply_filters('RoscoePropertyAmenities', 
											 	json_decode(get_post_meta($post->ID, 'propCommunityAmenities', true)), 
										        json_decode(get_post_meta($post->ID, 'amenities', true)), 
										        wp_get_post_terms($post->ID, 'prop_pet_restrictions', ['fields' => 'names']),
										        wp_get_post_terms($post->ID, 'prop_tags', ['fields' => 'names']),
										        get_field('comm_amenities', $post->ID),
										        get_field('apart_amenities', $post->ID)
										    );
										?> 
									</ul>

									<?php echo preg_match('/No Description/', $description) ? get_the_content() : '<p>'.trim($description).'</p>'; ?>

									<?php if ( have_rows('property_social_media') ) : ?>
										<ul id="social-links" class="social-links">
											<?php while ( have_rows('property_social_media') ) : the_row(); ?>
												<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2170">
													<a 
														href="<?php the_sub_field('social_media_profile_url'); ?>" 
														title="<?php the_sub_field('social_media_profile_url'); ?>"
													>icon</a>
												</li>
											<?php endwhile; ?>
										</ul>
									<?php endif; wp_reset_postdata(); ?>
									
								</div>

								<div class="tabs-panel" id="panel1v">
									<rent-press-property-floorplans prop-code="<?php echo get_post_meta($post->ID, 'prop_code', true); ?>">
										<header class="filter-controls">
											<form action="" class="clearfix medium-collapse" ng-init="flashFilters.beds=[];">
												<div class="medium-8 columns">

													<h3>Filter by bedrooms</h3>
													<div class="btn-group-wrapper">
														<label 
															class="-checkbox-wrapper" 
															ng-class="{'is-active': flashFilters.beds.length==0}" 
															ng-click="flashFilters.beds=[];"
														>
															<input type="checkbox" ng-model="flashFilters.beds.length" ng-true-value="0"> All
														</label>

														<label 
															class="-checkbox-wrapper" 
															ng-repeat="bed in values_of_filters_from_floorplans.beds | orderBy" 
															ng-class="{'is-active': flashFilters.beds.indexOf(bed)!=-1}"
														>
															<input type="checkbox" checklist-model="flashFilters.beds" checklist-value="bed">
															{{ bed | text_for_beds:false:false }} 
														</label>
													</div>
												</div>
												<aside class="medium-4 columns">
													<h3>Sort by</h3>
													<select ng-model="flashFilters.sort">
														<option value="_title:asc">Default</option>
														<option value="meta_data.fpMaxRent:asc">$ Min - Max</option>
														<option value="meta_data.fpMaxRent:desc">$ Max - Min</option>
														<option value="from_available_units.numOfAvailable:desc">Apartments Available</option>
														<option value="from_available_units.soonestAvailablity:desc">Soonest Available</option>
													</select>
												</aside>
											</form>
										</header>

										<section class="floorplan-grid clearfix">
											<div ng-if="filteredFloorplans.length == 0 && ! gathering_from_api">
												No available floor plans were found with the current selected options
											</div>

											<!-- <div ng-if="gathering_from_api == true">
												Loading...
											</div> -->

											<div>
												<div class="is-floorplan" ng-repeat="floorplan in filteredFloorplans = ( floorplans | flashFilterFloorplans:flashFilters )">
													<figure class="is-photo">
														<a href="{{ floorplan.permalink }}">
															<img ng-src="{{ floorplan.image }}" alt="">
														</a>
													</figure>
													<section class="floorplan-data">
														<h4>{{ floorplan._title }}</h4>

														<!-- Show if rent is more than 0 -->
														<h6 ng-if="floorplan.meta_data.fpMinRent !== -1">
															From ${{ floorplan.meta_data.fpMinRent }}/MO.
														</h6>

														<!-- Show if rent is less than or equal to 0 -->
														<h6 ng-if="floorplan.meta_data.fpMinRent === -1">
															Call for Pricing
														</h6>

														<p class="is-count">
															<span>{{ floorplan.meta_data.fpBeds | text_for_beds:false:false }}</span> | 
															<span>{{ floorplan.meta_data.fpBaths | text_for_baths:false }}</span> | 
															<span>{{ floorplan.meta_data.fpMinSQFT }} Sq. Ft.</span> 
														</p>

														<p class="fp-available-unit-count" ng-if="floorplan.from_available_units.numOfAvailable != 0">
															{{ floorplan.from_available_units.numOfAvailable }} Available Now
														</p>

														<p class="fp-available-unit-count" ng-if="floorplan.from_available_units.numOfAvailable == 0">
															No Apartments Available
														</p>

														<a href="{{ floorplan.permalink }}" class="button">View Details</a>
													</section>
												</div> <!-- is-floorplan -->									
											</div>
										</section>
									</rent-press-property-floorplans>
								</div>
								
								
								<div class="tabs-panel amenity-tab" id="panel2v">

									<?php get_template_part('template-parts/content', 'amenity-module'); ?>

								</div>

								<?php 
									$hoodPostID = get_post_meta($post->ID, 'post_type_connected_to_neighborhoods', true);

									if ( $hoodPostID ) :
									    $hood = get_post($hoodPostID); 
										$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($hoodPostID)) ?: 'http://placehold.it/1000x700&text=Neighborhood Photo'; ?>
										<div class="tabs-panel" id="panel3v">
											<div class="clearfix medium-collapse">
												<section class="medium-6 columns hood-tab-data" ng-mouseenter="refreshNeighborhoodMap();">
													<div class="is-data-wrapper">
														<h3><?php echo $hood->post_title; ?></h3>
														<?php 
														if ( get_field('neighborhood_romance_copy') ) :  
															the_field('neighborhood_romance_copy');  
														else : 
															echo apply_filters('the_content', $hood->post_content);
														endif; ?>
													</div>
												</section>
												<aside class="medium-6 columns hood-tab-map">
													<?php $mapArguments = [
														'post_type' => 'properties',
														'post_status' => 'publish',
														'post__not_in' => [ $post->ID ],
														'properties_from_neighborhood' => $hoodPostID,
													]; ?>

											    	<rent-press-properties init-query-args='<?php echo json_encode($mapArguments); ?>'>
														<ui-gmap-google-map center='ui_map.center' zoom='ui_map.zoom' options="NeighborhoodMapOptions">
														    <ui-gmap-markers 
														        models="filteredProperties" 
														        idKey="'ID'" 
														        coords="'self'" 
														        icon="'icon'"
														        doCluster="true" 
														        clusterOptions="ui_map_clusterOptions" 
														        fit="true"
														        click="ui_map_marker_click"
														        control="NeighborhoodMapMarkerControl"
														    >
														        <ui-gmap-windows>
														            <div ng-non-bindable>
														              <h4><a href="{{ permalink }}">
																			<span itemprop="name">{{ _title }}</span>
																		</a></h4>

																		<div class="is-comm-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
																		    <span itemprop="streetAddress">{{ meta_data.propAddress }}</span>
																		    </br>
																		    <span itemprop="addressLocality">{{ meta_data.propCity }}</span>,
																		   	<span itemprop="addressRegion">{{ meta_data.propState }}</span>
																		   	<span itemprop="postalCode">{{ meta_data.propZip }}</span>
																		</div>

																		<p>
																			<a href="tel: {{ meta_data.propPhoneNumber }}">
																				<span class="fa fa-phone"></span> <span itemprop="telephone">{{ meta_data.propPhoneNumber }}</span>
																			</a>
																			<br>
																			<a href="mailto: {{ meta_data.propEmail }}">
																				<span class="fa fa-envelope"></span> <span itemprop="email">Email</span>
																			</a>
																		</p>

																		{{ availableAtEachBedLevel | html_for_availableAtEachBedLevel }}
														            </div>
														        </ui-gmap-windows>
														    </ui-gmap-markers>
														</ui-gmap-google-map>
											    	</rent-press-properties>
													
													<style>.angular-google-map-container { display: block; height: 600px; }</style>
												</aside>
											</div>

											<section 
												class="hood-photo has-bg-img" 
												style="background-image: url('<?php echo esc_url($featuredImage); ?>');"
											></section>
										</div><?php 
									endif; 
								?>
							</div>
						</div>
					</div>
				</section>
						
				<section class="headline-strip">
					<h2 class="is-headline">Exceed Every Need</h2>
				</section>

				<!-- Property Gallery Area -->
				<?php 
					$gallery = get_field('property_gallery'); 
					if ( $gallery ) : ?>
					<section id="gallery" class="prop-slider has-slider clearfix small-collapse">
						
						<span class="is-top-accent"></span>

						<div class="slider-main-photo medium-8 large-9 columns">
							<?php if ( have_rows('property_gallery') ) : while ( have_rows('property_gallery') ) : the_row(); ?>
								<div class="slide has-bg-img" style="background-image: url('<?php the_sub_field('image'); ?>')"></div>
							<?php endwhile;endif;wp_reset_postdata(); ?>
						</div>
						
						<aside class="slider-thumbnails medium-4 large-3 columns show-for-medium">
							<?php if ( have_rows('property_gallery') ) : while ( have_rows('property_gallery') ) : the_row(); ?>
								<div class="is-thumb has-bg-img" style="background-image: url('<?php the_sub_field('image'); ?>')"></div>
							<?php endwhile;endif;wp_reset_postdata(); ?>
						</aside>

					</section>
				<?php endif; ?>

				<section class="headline-strip is-white">
					<h2 class="is-headline">Nearby Communities</h2>
				</section>

				<?php get_template_part( 'template-parts/content', 'prop-grid' ); ?>
				
			</main>

			<?php get_template_part( 'template-parts/content', 'cta-banner' ); ?>

		<?php endwhile; ?>
	</div>

	<script type="text/javascript">
		var mapNeighborhood={};

		var angularTemplateApp=angular.module('TemplateTabWrapper', ['rentPress']);

		angularTemplateApp.controller('mainCtrl', ['$scope', '$rootScope', 'uiGmapGoogleMapApi','uiGmapIsReady', 'uiGmapFitHelper', function($scope, $rootScope, uiGmapGoogleMapApi, uiGmapIsReady, uiGmapFitHelper) {
			$scope.NeighborhoodMap={};

			$scope.NeighborhoodMapMarkerControl={};

			$scope.NeighborhoodMapOptions={
				'_id': 'single-prop-neighborhood', 
				'maxZoom': 18,
				styles: [
				    {
				        "featureType": "administrative",
				        "elementType": "labels.text.fill",
				        "stylers": [
				            {
				                "color": "#444444"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "all",
				        "stylers": [
				            {
				                "color": "#f2f2f2"
				            }
				        ]
				    },
				    {
				        "featureType": "poi",
				        "elementType": "all",
				        "stylers": [
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "all",
				        "stylers": [
				            {
				                "saturation": -100
				            },
				            {
				                "lightness": 45
				            },
				            {
				                "color": "#ca956d"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "labels.text",
				        "stylers": [
				            {
				                "visibility": "simplified"
				            },
				            {
				                "color": "#000000"
				            }
				        ]
				    },
				    {
				        "featureType": "road.highway",
				        "elementType": "all",
				        "stylers": [
				            {
				                "visibility": "simplified"
				            }
				        ]
				    },
				    {
				        "featureType": "road.arterial",
				        "elementType": "labels.icon",
				        "stylers": [
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "transit",
				        "elementType": "all",
				        "stylers": [
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "all",
				        "stylers": [
				            {
				                "color": "#3f8aad"
				            },
				            {
				                "visibility": "on"
				            }
				        ]
				    }
				]
			};

			uiGmapIsReady.promise().then(function(instances) {
		        instances.forEach(function(inst) {
		            var map = inst.map;
		            var uuid = map.uiGmap_id;
		            var _id = map._id;
		            var mapInstanceNumber = inst.instance; // Starts at 1.

		            if (_id == 'single-prop-neighborhood') {
		            	$scope.NeighborhoodMap=map;
		            	mapNeighborhood=map;
						google.maps.event.trigger(map, 'resize');
		            }
		        });
		    });

			$scope.refreshNeighborhoodMap = function() {

				uiGmapGoogleMapApi.then(function(google_maps) {
					google_maps.event.trigger($scope.NeighborhoodMap, 'resize');

					var markers=$scope.NeighborhoodMapMarkerControl.getGMarkers();

					uiGmapFitHelper.fit(markers, $scope.NeighborhoodMap);
			    });
			};   

			document.addEventListener("DOMContentLoaded", function(event) {
				document.querySelector('a[href="#panel3v"]').onclick=function() {
					uiGmapGoogleMapApi.then(function(google_maps) {
						google_maps.event.trigger($scope.NeighborhoodMap, 'resize');

						var markers=$scope.NeighborhoodMapMarkerControl.getGMarkers();

						uiGmapFitHelper.fit(markers, $scope.NeighborhoodMap);
				    });
				};
			});
			
	    }]);

	    // init rentpress angular for map
		var rentPress_App = angular.bootstrap(
			document.getElementById('angular-tp-single-prop-wrapper'), 
			['TemplateTabWrapper']
		);
	</script>
<?php get_footer(); ?>