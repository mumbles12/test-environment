(function($) {
	$(document).ready(function() {
		// Single property pricing and availability section
		$('.hide-leftover-units').on('click', function() {
			$(this).hide();
			$(this).parent().find('.view-leftover-units').show();
			var floorPlanContainer = $(this).closest('.medium-bed-container').find('.floorplans-container');
			floorPlanContainer.find('.ajax-unit').remove();
		});
		$('.view-leftover-units').on('click', function(e) {
			e.preventDefault();
			$(this).hide();
			$(this).parent().find('.hide-leftover-units').show();
			var visibleUnits = [];
			// Fetch units that are already visible so we dont include them
			$('.is-fp.-bed-'+$(this).data('bed-count')).each( function () {
				if ( $(this).is(':visible') ) {
					visibleUnits.push($(this).data('unit-name'));
				}
			});
			$.ajax({
				url: rentpress_unit_data.base_url + 'units/from/property',
				method: 'POST',
				beforeSend: function(xhr) {
					xhr.setRequestHeader( 'X-WP-Nonce', rentpress_unit_data.nonce );
				},
				data: {
					property_post_id: $(this).data('property-post-id'),
					num_beds: $(this).data('bed-count'),
					exclude_units: visibleUnits
				}
			}).done(function(data) {
				// Loop through units in request response data and display them with their main
				// unit ( apartment ) group
				$.each(data, function( index, unit ) {
					displayUnitItem(unit);
				});

				$('.is-fp').each( function(){
					var $this 		= $(this),
						$detailsLink= $this.find('.fp-details-link'),
						$thumb		= $this.find('.fp-thumb').contents().clone(),
						$desc		= $this.siblings('.prop-description').contents().clone(),
						$avail		= $this.find('.fp-details-header').contents().clone(),
						$unit		= $this.find('.fp-unit').contents().clone(),
						$amenities 	= $this.find('.a-list').contents().clone(),
						$cta 		= $this.find('.prop-cta').contents().clone(),
						$videoLink  = $this.find('.watch-video-link').data('videoid'),
						$gallery 	= $this.find('.fp-prev-gall').contents().clone();

						$('.watch-video-link').each( function() {
					    	$(this).magnificPopup({
						        // disableOn: 700,
						        type: 'iframe',
						        mainClass: 'mfp-fade',
						        removalDelay: 160,
						        preloader: false,
						        fixedContentPos: false
					        });
						});

						$('.watch-360-link').each( function() {
					    	$(this).magnificPopup({
						        // disableOn: 700,
						        type: 'iframe',
						        mainClass: 'mfp-fade',
						        removalDelay: 160,
						        preloader: false,
						        fixedContentPos: false
					        });
						});

						$('.launch-img-tour').each( function(){
							$(this).on( 'click', function() {

							    var popupSRC = $(this).closest('.fp-aside').siblings('.fp-prev-gall');
							        $.magnificPopup.open({
								        items: {
								            src: popupSRC,
								        },
							        type: 'inline',

							        callbacks: {
									    open: function() {
									      $('body').find('.mfp-content .fp-prev-gall-wrapper').not('.slick-initialized').slick();
									    },
									    // close: function() {
									    // 	$('.modal-data-box').removeClass('is-active').empty();
									    // }
									  }

							    });
							});
						}); // launch-img-tour

					$detailsLink.magnificPopup({
						disableOn: 640,
						callbacks: {
						    open: function() {
						    	
								var $modal = $('#fpDetailsModal');

								$modal.find('.fp-thumb').html($thumb);
								$modal.find('.fpm-prop-description').html($desc);
								$modal.find('.fp-details-header').html($avail);
								$modal.find('.fp-unit').html($unit);
								$modal.find('.a-list').html($amenities);
								$modal.find('.prop-cta').html($cta);
								$modal.find('.is-video-tour').attr('data-video', $videoLink);
								$modal.find('.is-360').attr('data-video', $videoLink); // will need to be changed when we figure out what this 360 stuff is
								// need to add other 2 icon links


								$modal.find('.video-icon').each( function() { // moved inside fp-details.each

									$(this).on( 'click', function() {
										
										$(this).closest('.is-fp').siblings('.modal-data-box').addClass('is-active');

										var $vid = $(this).data('video');

										if( $(this).hasClass('is-360') ) {
											var $videoDiv = '<div class="responsive-embed widescreen"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + $vid + '" frameborder="0" allowfullscreen></iframe></div>';
											$('#loadData').html($videoDiv);
										}

										if( $(this).hasClass('is-video-tour') ) {
											var $videoDiv = '<div class="responsive-embed widescreen"><iframe width="560" height="315" src="https://www.youtube.com/embed/' + $vid + '" frameborder="0" allowfullscreen></iframe></div>';
											$('#loadData').html($videoDiv);
										}

										if( $(this).hasClass('is-img-gallery') ) {

											// if ( !$('#loadData').find('.fp-prev-gall-wrapper').length > 0 ) {
												// $gallery.appendTo('#loadData');
												$('#loadData').html($gallery);
												if ( $('#loadData').find('.fp-prev-gall-wrapper').length > 0 ) {
													$('#loadData').find('.fp-prev-gall-wrapper').not('.slick-initialized').slick();
												}
											// }
										}


										$('.modal-data-box').children('.fa-close').on( 'click', function() {
											$(this).parent('.modal-data-box').removeClass('is-active');
											// if ( $('#loadData').find('.fp-prev-gall-wrapper').length > 0 ) {
											// 	console.log('has gallery');
										 //    	$('#loadData').find('.slick-initialized').unslick();
										 //    }
											$('#loadData').empty();
										});


									});

								}); // end video-icon.each

								$('.watch-video-link').each( function() {
							    	$(this).magnificPopup({
								        // disableOn: 700,
								        type: 'iframe',
								        mainClass: 'mfp-fade',
								        removalDelay: 160,
								        preloader: false,
								        fixedContentPos: false
							        });
								});

								$('.watch-360-link').each( function() {
							    	$(this).magnificPopup({
								        // disableOn: 700,
								        type: 'iframe',
								        mainClass: 'mfp-fade',
								        removalDelay: 160,
								        preloader: false,
								        fixedContentPos: false
							        });
								});

								$('.launch-img-tour').each( function(){
									$(this).on( 'click', function() {
										console.log('heyyyy');
									    var popupSRC = $(this).closest('.fp-aside').siblings('.fp-prev-gall');
									        $.magnificPopup.open({
										        items: {
										            src: popupSRC,
										        },
									        type: 'inline',

									        callbacks: {
											    open: function() {
											      $('body').find('.mfp-content .fp-prev-gall-wrapper').not('.slick-initialized').slick();
											    },
											    // close: function() {
											    // 	$('.modal-data-box').removeClass('is-active').empty();
											    // }
											  }

									    });
									});
								}); // launch-img-tour

						    },
						    close: function() {
						    	$('.modal-data-box').removeClass('is-active').find('#loadData').empty();
						    	if ( $('#loadData').find('.fp-prev-gall-wrapper').length > 0 ) {
							    	$('.modal-data-box').find('.fp-prev-gall-wrapper.slick-initialized').unslick();
							    }
						    }
						 }
					});
				}); // end each is-fp

			}).fail(function(jqXHR, textStatus, errorThrown) {
				if ( 'undefined' != typeof jqXHR.responseJSON.message ) {
					alert(jqXHR.responseJSON.message);
					return;
				}
				console.log('Failure: ' + textStatus);
			});
		});

		function displayUnitItem(unit) {
			var unitDetails = {
				favorited: false,
				name: unit.Information.Name,
				price: unit.Rent.MarketRent,
				squareFeet: unit.SquareFeet.Min,
				beds: unit.Rooms.Bedrooms,
				baths: unit.Rooms.Bathrooms,
				images: unit.Images,
				videos: unit.Videos,
				floorPlanImage: unit.Image.FloorPlanImage,
				floorlevel: unit.Rooms.floorlevel,
			};
			$newUnitItem = createUnitItem(unitDetails);
		}

		function createUnitItem(unitDetails) {
			var unitOutputContainer = $('.medium-bed-container.bed-'+unitDetails.beds+' .floorplans-container'); 
			var unitClone = unitOutputContainer.children().eq(0).clone();
			unitClone.addClass('ajax-unit');
			unitClone.find('.unit-name').text(unitDetails.name);
			unitClone.find('.unit-sq-ft').text(unitDetails.squareFeet + ' SF');
			if (unitDetails.floorlevel) {
				unitClone.find('.unit-floorlevel').text('Floor Level: '+ unitDetails.floorlevel);
			}
			unitClone.find('.fp-details-header').text('$' + numberWithCommas(unitDetails.price));

			if (unitDetails.images[0]) {
				//unitClone.find('.fp-img').attr('src', unitDetails.images[0].url);
			}
			
			unitClone.find('.fp-img').attr('src', 'http://placehold.it/500x500&text=ThisFloorPlan');
			
			// unitClone.find('.launch-360 a').attr('href', unitDetails.videos[0].url);
			// unitClone.find('.launch-video-tour a').attr('href', unitDetails.videos[0].url);
			unitClone.find('.launch-360 a').attr('href', 'https://www.youtube.com/watch?v=8EkTnc66gjg');
			unitClone.find('.launch-video-tour a').attr('href', 'https://www.youtube.com/watch?v=8EkTnc66gjg');
			unitClone.find('.fp-prev-gall-wrapper').html(createImageGallerySlides(unitDetails.images));
			unitOutputContainer.append(unitClone);
		}

		function createImageGallerySlides(images) {
			var html = '';
			$.each(images, function( image, key ) {
				// html.concat('<div><img src="'+image.url+'"/></div>');
				html = html.concat('<div><img src="http://placehold.it/500x500&text=ThisFloorPlan"/></div>');
			});
			return html;
		}

		function numberWithCommas(x) {
		    var parts = x.toString().split(".");
		    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    return parts.join(".");
		}
	});
})(jQuery);