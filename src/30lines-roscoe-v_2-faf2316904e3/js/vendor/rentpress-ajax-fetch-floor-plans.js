(function($) {
	$(document).ready(function() {
		// Single property pricing and availability section
		// $('.hide-unavailable-floor-plans').on('click', function() {
		// 	$(this).hide();
		// 	// code...
		// });
		$('.view-unavailable-floor-plans').on('click', function(e) {
			e.preventDefault();
			var bedCount = $(this).data('bed-count');
			var unavailableFloorPlanContainer = $('.bed-'+bedCount).find('#unavailableFloorPlans_'+bedCount);
			if ( $('body').find('#unavailableFloorPlans_'+bedCount+' .fp-grid-item').length === 0 ) {
				// $(this).parent().after('<div id="unavailableFloorPlans_'+bedCount+'" class="columns small-12"><span class="fa fa fa-circle-o-notch fa-spin fa-3x fa-fw"></span></div>');
				$.ajax({
					url: rentpress_floorplan_data.base_url + 'floorplans/from/property',
					method: 'POST',
					beforeSend: function(xhr) {
						xhr.setRequestHeader( 'X-WP-Nonce', rentpress_floorplan_data.nonce );
					},
					data: {
						property_post_id: $(this).data('property-post-id'),
						num_beds: bedCount,
						unavailable_only: true
					}
				}).done(function(data) {
					unavailableFloorPlanContainer.html(' ');
					// Loop through floor plans in request response data and display them in a custom container below the main unit group above it 
					if ( data.length > 0 )  {
						$.each(data, function( index, floorplan ) {
							displayFloorPlanGridItem(floorplan, bedCount);
						});
					} else {
						unavailableFloorPlanContainer.html('<h4>No unavailable floor plans at the moment.</h4>')
					}
					$('.bed-'+bedCount).find('.view-unavailable-floor-plans').text('Hide Unavailable Floor Plans');
					$('.bed-'+bedCount).find('.view-unavailable-floor-plans').addClass('hide-unavailable-floor-plans');
					unavailableFloorPlanContainer.show();
				}).fail(function(jqXHR, textStatus, errorThrown) {
					if ( 'undefined' != typeof jqXHR.responseJSON.message ) {
						alert(jqXHR.responseJSON.message);
						return;
					}
					console.log('Failure: ' + textStatus);
				});
			} else {
				if ( $(this).hasClass('hide-unavailable-floor-plans') ) {
					var unavailableFloorPlanContainer = $('.bed-'+bedCount).find('#unavailableFloorPlans_'+bedCount);
					unavailableFloorPlanContainer.css('display', 'none');
					$(this).text('VIEW UNAVAILABLE FLOORPLANS & BE NOTIFIED +');
					$(this).removeClass('hide-unavailable-floor-plans') 
				} else {
					unavailableFloorPlanContainer.css('display', 'block');
					$(this).text('Hide Unavailable Floor Plans');
					$(this).addClass('hide-unavailable-floor-plans') 
				}
			}
		});

		function displayFloorPlanGridItem(floorPlan, bedCount) {
			var unavailFloorPlanGridContainer = $('body').find('#unavailableFloorPlans_'+bedCount); 
			var fpGridItem = ''.concat(
				'<div class="fp-grid-item columns small-12 medium-4">',
					'<h4>'+ floorPlan.fpName +'</h4>',
					'<span>'+ floorPlan.fpMinSQFT +' SF - '+ floorPlan.fpMaxSQFT +' SF</span>',
					'<img width="100%" src="'+ floorPlan.fpImg +'"/>',
					'<a href="'+ floorPlan.fpAvailURL +'" class="button">Request Info</a>',
				'</div>' // End fp-grid-item
			);
			unavailFloorPlanGridContainer.append(fpGridItem);
		}

		function numberWithCommas(x) {
		    var parts = x.toString().split(".");
		    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    return parts.join(".");
		}
	});
})(jQuery);