(function($) {
	$(document).ready(function() {
		// Single property pricing and availability section

		$('.mobile-fp-accordion .btn-more').on('click', function(_event) {
			_event.preventDefault();
			//$(this).hide();
			//$(this).parent().find('.hide-leftover-units').show();
			var visibleUnits = [];
			// Fetch units that are already visible so we dont include them
			$('.mobile-fp-accordion .is-fp.-bed-'+$(this).data('bed-count')).each( function () {
				if ( $(this).is(':visible') ) {
					visibleUnits.push($(this).data('unit-name'));
				}
			});

			//console.log(visibleUnits);

			$.ajax({
				url: rentpress_unit_data.base_url + 'units/from/property',
				method: 'POST',
				beforeSend: function(xhr) {
					xhr.setRequestHeader( 'X-WP-Nonce', rentpress_unit_data.nonce );
				},
				data: {
					property_post_id: $(this).data('property-post-id'),
					num_beds: $(this).data('bed-count'),
					exclude_units: visibleUnits,
					units_per_page: 3,
				}
			}).done(function(data) {
				// Loop through units in request response data and display them with their main
				// unit ( apartment ) group

				$.each(data, function( index, unit ) {
					displayUnitItem(unit);
				});
				
				var visibleUnits = [];
				// Fetch units that are already visible so we dont include them
				$('.mobile-fp-accordion .is-fp.-bed-'+$(_event.target).data('bed-count')).each( function () {
					if ( $(this).is(':visible') ) {
						visibleUnits.push($(this).data('unit-name'));
					}
				});

				if (data.length == 0 || $(_event.target).data('total-units') == visibleUnits.length ) {
					$(_event.target).hide();
				}

				$('.fp-details-link-mobile').each(function(){
					$(this).on('click', function(){
						$(this).toggleClass('is-active');
						$(this).parent('.fp-details').siblings('.mobile-hidden').toggleClass('is-active');
					});

					$(this).one('click', function(){
						$(this).parent('.fp-details').siblings('.mobile-hidden').children('.fp-mobile-gallery').slick();
					});
				});

				$('.fp-close-details-link-mobile').each(function() {
					$(this).on('click', function(){

						var is_fp=$(this).parent().parent().parent();

						$('.fp-details-link-mobile', is_fp).removeClass("is-active");

						$('html, body').animate({
					        scrollTop: $(this).parent().parent().offset().top - 200,
					    }, 250);
					    
						$(this).parent().parent().removeClass('is-active');
					});
				});

				$('.more-amenities-link').on('click', function(){
			    	$(this).toggleClass('is-active').siblings('.a-list').toggleClass('is-active');
			    });

			    $('.watch-video-link').each( function() {
			    	$(this).magnificPopup({
				        // disableOn: 700,
				        type: 'iframe',
				        mainClass: 'mfp-fade',
				        removalDelay: 160,
				        preloader: false,
				        fixedContentPos: false
			        });
				});

				$('.watch-360-link').each( function() {
			    	$(this).magnificPopup({
				        // disableOn: 700,
				        type: 'iframe',
				        mainClass: 'mfp-fade',
				        removalDelay: 160,
				        preloader: false,
				        fixedContentPos: false
			        });
				});
			}).fail(function(jqXHR, textStatus, errorThrown) {
				if ( 'undefined' != typeof jqXHR.responseJSON.message ) {
					alert(jqXHR.responseJSON.message);
					return;
				}
				console.log('Failure: ' + textStatus);
			});
		});

		function displayUnitItem(unit) {
			var unitDetails = {
				favorited: false,
				name: unit.Information.Name,
				price: unit.Rent.MarketRent,
				squareFeet: unit.SquareFeet.Min,
				beds: unit.Rooms.Bedrooms,
				baths: unit.Rooms.Bathrooms,
				images: unit.Images,
				videos: unit.Videos,
				amenities: unit.Amenities,
				floorPlanImage: unit.Image.FloorPlanImage,

				floorlevel: unit.Rooms.floorlevel,
			};
			$newUnitItem = createUnitItem(unitDetails);
		}

		function createUnitItem(unitDetails) {
			var unitOutputContainer = $('.mobile-fp-accordion #mobile-beds-'+unitDetails.beds+' .floorplans'); 

			var unitClone = $('.is-fp:eq(0)', unitOutputContainer).clone();
				unitClone.removeClass('is-active');

			unitClone.attr('data-unit-name', Number(unitDetails.name));

			unitClone.addClass('ajax-unit');
			//unitClone.find('.unit-name').text(unitDetails.name);
			unitClone.find('.unit-sq-ft').text(unitDetails.squareFeet + ' SF');
			if (unitDetails.floorlevel) {
				unitClone.find('.unit-floorlevel').text('Floor Level: '+ unitDetails.floorlevel);
			}
			unitClone.find('.fp-details-header').text('$' + numberWithCommas(unitDetails.price));
			if (unitDetails.images[0] && unitDetails.images[0].url) {
				//unitClone.find('.fp-thumb img').attr('src', unitDetails.images[0].url);
			}
			else if (unitDetails.floorPlanImage) {
				//unitClone.find('.fp-thumb img').attr('src', unitDetails.floorPlanImage);
			}

			unitClone.find('.fp-thumb img').attr('src', '<img src="http://placehold.it/500x500&text=ThisFloorPlan" />');

			// unitClone.find('.launch-360 a').attr('href', unitDetails.videos[0].url);
			// unitClone.find('.launch-video-tour a').attr('href', unitDetails.videos[0].url);
			// unitClone.find('.launch-360 a').attr('href', 'https://www.youtube.com/watch?v=8EkTnc66gjg');
			//unitClone.find('.launch-video-tour a').attr('href', 'https://www.youtube.com/watch?v=8EkTnc66gjg');
			unitClone.find('.fp-mobile-gallery').html(createImageGallerySlides(unitDetails.images));

			unitClone.find('.unit-amenities ul').html('');

			unitDetails.amenities.forEach(function(a) {

				unitClone.find('.unit-amenities ul').append('<li>'+ a +'</li>');

			});

			unitClone.find('.fp-details-link-mobile').removeClass('is-active');
			unitClone.find('.fp-details-link-mobile').parent('.fp-details').siblings('.mobile-hidden').removeClass('is-active');
				
			unitOutputContainer.append(unitClone);
		}

		function createImageGallerySlides(images) {
			var html = '';
			$.each(images, function( image, key ) {
				//html.concat('<div><img src="'+image.url+'"/></div>');
				html.concat('<div><img src="http://placehold.it/500x500&text=ThisFloorPlan"/></div>');
			});
			return html;
		}

		function numberWithCommas(x) {
		    var parts = x.toString().split(".");
		    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    return parts.join(".");
		}
	});


}(jQuery));