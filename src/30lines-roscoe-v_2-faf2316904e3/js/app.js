(function($) {

	$(document).foundation();

	$(document).ready(function(){

		// * page scrolling http://css-tricks.com/snippets/jquery/smooth-scrolling/
		$(function() {
		  	$('a.smooth-scroll').click(function() {
		    	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      	var target = $(this.hash);
			      	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      	if (target.length) {
			        	$('html, body').animate({
			          		scrollTop: target.offset().top
			        	}, 300);
			        	return false;
			      	}
		    	}
		  	});
		});
		// * Page scrolling

		$(function() {
			// * Scroll Nav - https://jsfiddle.net/mariusc23/s6mLJ/31/
			var didScroll,
				lastScrollTop = 0,
				delta = 5,
				navbarHeight = $('.main-nav').outerHeight();

			$(window).scroll(function(event){
			    didScroll = true;
			});

			setInterval(function() {
			    if (didScroll) {
			        hasScrolled();
			        didScroll = false;
			    }
			}, 250);

			function hasScrolled() {
			    var st = $(this).scrollTop();
			    // * Make sure they scroll more than delta
			    if(Math.abs(lastScrollTop - st) <= delta)
			        return;

			    // * If they scrolled down and are past the navbar, add class .nav-up.
			    // * This is necessary so you never see what is "behind" the navbar.
			    if (st > lastScrollTop && st > navbarHeight){
			        // Scroll Down
			        $('.nav-down').removeClass('nav-down').addClass('nav-up');
			    } else {
			        // * Scroll Up
			        if(st + $(window).height() < $(document).height()) {
			            $('.nav-up').removeClass('nav-up').addClass('nav-down');
			        }
			    }
			    lastScrollTop = st;
			}
			// check for nav-down class and browser width -> add padding to body to compensate
			if( $('.main-nav').hasClass('nav-down') && $(window).width() >= 768 ) {
				$('body').addClass('has-fixed-nav');
			}
			// achieving this with css min-height on .main-header at the moment
			// * End scroll nav
		});

			// Main Nav
		$('#navToggle').on('click', function(){

			var toggle = $(this);

			toggle.toggleClass('is-active');
			toggle.siblings('.menu').toggleClass('is-active');
		});

		// $('.checkbox-wrapper').each( function(){
		// 	$(this).find('input').on( 'click', function(){
		// 		$(this).parent('.checkbox-wrapper').toggleClass('is-active');
		// 	});
		// });

		$('#sortPrice').on('click', function(e){

			var $this = $(this);

			$this.toggleClass('is-active');
			$this.text($(this).text() == 'Min Price' ? 'Max Price' : 'Min Price');
			e.preventDefault();
		});

		if ( $('.has-slider').length ) {

			//single prop slider
			$('.slider-main-photo').slick({
				slidesToShow: 1,
			  	slidesToScroll: 1,
				arrows: false,
				dots: false,
				asNavFor: '.slider-thumbnails'
			});

			$('.slider-thumbnails').slick({
			    arrows: true,
				dots: false,
			    vertical: true,
			    verticalSwiping: true,
			    slidesToShow: 1,
			    centerMode: true,
			    focusOnSelect: true,
			    slidesToScroll: 1,
			    asNavFor: '.slider-main-photo'
			  });

		}

		if ($('body').hasClass('page-template-template-advanced-search')) {

			$('.view-toggles .fa').on('click', function(){
				var $this = $(this);

				$this.addClass('is-active').siblings('.fa').removeClass('is-active');

				if( $this.is('#viewRow') ) {
					$('#gridContainer').removeClass('is-grid-v').addClass('is-row-v is-active');
					$('#mapResults').removeClass('is-active');
				} else {
					$('#gridContainer').removeClass('is-row-v').addClass('is-grid-v is-active');
					$('#mapResults').removeClass('is-active');
				}

				if( $this.is('#viewMap') ) {
					$('#gridContainer').removeClass('is-active');
					$('#mapResults').addClass('is-active');
				}
			});
		}

		if( $('#amenityModule').length ) {
			$('.community-bullet-points li.has-photo').first().addClass('is-active');
			$('#amenityGallery .has-bg-img').first().addClass('is-active');

			$('#communitySelect').on('click', function(){
				$('#apartmentSelect').removeClass('is-active');
				$(this).addClass('is-active');

				$('.apartment-bullet-points').removeClass('is-active');
				$('.community-bullet-points').addClass('is-active');
			});

			$('#apartmentSelect').on('click', function(){
				$('#communitySelect').removeClass('is-active');
				$(this).addClass('is-active');

				$('.community-bullet-points').removeClass('is-active');
				$('.apartment-bullet-points').addClass('is-active');
			});

			$('.am-points li').each( function( i ) {
				if( $(this).attr('data-photo') ) {
					// console.log( $(this).attr('data-photo') );
					var $photoID = $(this).attr('data-photo'),
						$amGall  = $('#amenityGallery');

					$(this).on('click', function() {
						$('.am-points').find('li').removeClass('is-active');
						$(this).addClass('is-active');
						// console.log($photoID);
						$amGall.children('.is-active').removeClass('is-active');
						$amGall.children('#' + $photoID).addClass('is-active');
					});
				}
			});

			// $('#galleryTabs li').on('click', function() {
			// 	$(this).addClass('is-active').siblings().removeClass('is-active');
			// });

			$('#apartmentGalleryTab').on('click', function(){
				$(this).addClass('is-active').siblings().removeClass('is-active');
				$('#communityGalleryPanel').removeClass('is-active');
				$('#apartmentGalleryPanel').addClass('is-active');
			});

			$('#communityGalleryTab').on('click', function(){
				$(this).addClass('is-active').siblings().removeClass('is-active');
				$('#apartmentGalleryPanel').removeClass('is-active');
				$('#communityGalleryPanel').addClass('is-active');
			});
		}

		// Detect objectFit support
		// aliright, this is kinda progressive but damn it shouldn't be so hard to make images look good in perfectly shaped little boxes damn it
		if('objectFit' in document.documentElement.style === false) {
		// assign HTMLCollection with parents of images with objectFit to variable
		var container = document.getElementsByClassName('is-property');
			// Loop through HTMLCollection
			for (var i = 0; i < container.length; i++) {
				// Asign image source to variable
				var imageSource = container[i].querySelector('img').src;
				// Hide image
				container[i].querySelector('img').style.display = 'none';
				// Add background-size: cover
				container[i].style.backgroundSize = 'cover';
				// Add background-image: and put image source here
				container[i].style.backgroundImage = 'url(' + imageSource + ')';
				// Add background-position: center center
				container[i].style.backgroundPosition = 'center center';
			}
		}

			//check if svg is supported
		if(!document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#Image', '1.1')){
			var bodyElement = document.getElementsByTagName("body")[0];
		  	bodyElement.className += " noSvg";
		}

		$(window).load(function() {

			$('#multiple_locations_filter').multipleSelect({
				placeholder: 'Locations',

			});

		});

		// Load Front-Page Slider

		$('.fp-featured-properties').slick({
			dots: false,
			infinite: false,
			speed: 500,
			cssEase: 'linear',
			pauseOnHover: true,
			slidesToShow: 2,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});

		$('#careerCards').slick({
		  dots: true,
			arrows: false,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 640,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});

	$('.employee-block').each( function() {
		$(this).on('click', function() {
			$('.slides-wrapper-2').find('.employee-block').not(this).removeClass('is-active');
			$(this).toggleClass('is-active');
		});
	});

	$('.current-employees').slick({
		dots: true,
		arrows: false,
		infinite: false,
		// centerMode: true,
		// centerPadding: '60px',
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		// responsive: [
		// {
		//   breakpoint: 1200,
		//   settings: {
		//     slidesToShow: 2,
		//     slidesToScroll: 2
		//   }
		// },
		// {
		//   breakpoint: 640,
		//   settings: {
		//     slidesToShow: 1,
		//     slidesToScroll: 1
		//   }
		// }
		// ]
	});

	// $('.is-map-search-container .is-state').each( function() {
	// 	$state = $(this);

	// 	$state.on( 'click', function(){
	// 		$this = $(this);
	// 		$name = $this.data('state');


	// 		if ($(this).data("state") === 'Texas') {
	// 		    $('#stateTexasModal').addClass('is-active');
	// 		} else if ($(this).data("state") === 'Colorado') {
	// 		    $('#stateColoradoModal').addClass('is-active');
	// 		} else {
	// 		    $('#stateFocus').addClass('is-active');
	// 		}


	// 		console.log($name);

	// 		// console.log('map icon ' + $(".state-focus[data-state='$name']").find('.state-locations .map-icon').length );

	// 		$(".state-focus[data-state='Texas']").find('.state-locations .map-icon').each( function() {
	// 			$this = $(this);
	// 			// $city = $this.closest('li').data('city');
	// 			// console.log($city);

	// 			$this.on('click', function() {
	// 				$city = $this.parent('li').data('city');
	// 				console.log($city);
	// 			});

	// 		});


	// 	});

	// });

	// $('.back-to-map').on( 'click', function(){
	// 	$('.state-focus').removeClass('is-active');
	// });

	// function imgError(image) {
	//     image.onerror = "";
	//     image.src = "/img/marble-BG.jpg";
	//     return true;
	// }



	});//on ready
})( jQuery );
