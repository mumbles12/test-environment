<?php
/**
 * Template Name: Left Sidebar On Page Navigation
 *
 * @package 30_Lines_Properties
 */
$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
get_header(); ?>

	<header class="hero is-single-prop has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
		

	</header>

	<main id="main" role="main">

		<section class="is-left-navigation-content">

			<div class="clearfix small-collapse">

				<div class="large-4 columns">
					<ul class="vertical tabs" data-tabs id="example-tabs">
						<?php 
						$count = 0;
						if ( have_rows('navigation_content') ) : while ( have_rows('navigation_content') ) : the_row(); 
							$tabTextNormal = get_sub_field('tab_text');
							$slug = sanitize_title(get_sub_field('tab_text'));
							$isActive = $count == 0 ? 'is-active' : '';
							echo "<li class='tabs-title {$isActive}'><a href='#{$slug}' aria-selected='true'>{$tabTextNormal}</a></li>";
							$count++;
						endwhile;endif;wp_reset_postdata(); ?>
					</ul>
				</div>
				<div class="large-8 columns">
					<div class="tabs-content" data-tabs-content="example-tabs">

						<?php 
						$count = 0;
						if ( have_rows('navigation_content') ) : while ( have_rows('navigation_content') ) : the_row(); 
							$tabTextNormal = get_sub_field('tab_text');
							$slug = sanitize_title(get_sub_field('tab_text'));
							$isActive = $count == 0 ? 'is-active is-overview' : ''; ?>
							<div class="tabs-panel <?php echo $isActive; ?>" id="<?php echo $slug; ?>">

								<?php if ( get_sub_field('tab_content_title') ) : ?>
									<h3><?php the_sub_field('tab_content_title'); ?></h3>
								<?php endif; ?>

								<?php the_sub_field('tab_content'); ?>



								<?php if ( $count === 3 && is_page(21) ) : ?>













							<div class="leadership-loop">

								<?php // WP_Query arguments
									$args = array(
										'post_type'              => array( 'team' ),
										'team_branch'			 => 'executive-team',
										'nopaging'               => true,
										'posts_per_page'         => '25',
									);

									// The Query
									$query = new WP_Query( $args );

									// The Loop
									if ( $query->have_posts() ) : ?>

										<h4>Executive Team</h4>

									<?php while ( $query->have_posts() ) : $query->the_post(); ?>

											
									<div <?php post_class('is-leader clearfix'); ?>>
										<figure>
											<?php if ( has_post_thumbnail() ) {
											the_post_thumbnail('medium');
											} ?>
										</figure>
										<aside class="leader-info">
											<h3><?php the_title(); ?></h3>
											<h6><?php if( get_field('team_title') ) { the_field('team_title'); } ?>
												<?php if( get_field('region') ) { echo ' | ' . get_field('region'); } ?>
											</h6>
											<?php the_content(); ?>
										</aside>
									</div>
									
								<?php endwhile;endif;wp_reset_postdata(); ?>

							</div> <!-- leadership -->

							<div class="leadership-loop">

								<?php // WP_Query arguments
									$args = array(
										'post_type'              => array( 'team' ),
										'team_branch'			 => 'operations-team',
										'nopaging'               => true,
										'posts_per_page'         => '25',
									);

									// The Query
									$query = new WP_Query( $args );

									// The Loop
									if ( $query->have_posts() ) : ?>

										<h4>Operations Team</h4>

									<?php while ( $query->have_posts() ) : $query->the_post(); ?>

											
									<div <?php post_class('is-leader clearfix'); ?>>
										<figure>
											<?php if ( has_post_thumbnail() ) {
											the_post_thumbnail('medium');
											} ?>
										</figure>
										<aside class="leader-info">
											<h3><?php the_title(); ?></h3>
											<h6><?php if( get_field('team_title') ) { the_field('team_title'); } ?>
												<?php if( get_field('region') ) { echo ' | ' . get_field('region'); } ?>
											</h6>
											<?php the_content(); ?>
										</aside>
									</div>
									
								<?php endwhile;endif;wp_reset_postdata(); ?>

							</div> <!-- leadership -->

							<div class="leadership-loop">

								<?php // WP_Query arguments
									$args = array(
										'post_type'              => array( 'team' ),
										'team_branch'			 => 'administrative-team',
										'nopaging'               => true,
										'posts_per_page'         => '25',
									);

									// The Query
									$query = new WP_Query( $args );

									// The Loop
									if ( $query->have_posts() ) : ?>

										<h4>Administrative Team</h4>

									<?php while ( $query->have_posts() ) : $query->the_post(); ?>

											
									<div <?php post_class('is-leader clearfix'); ?>>
										<figure>
											<?php if ( has_post_thumbnail() ) {
											the_post_thumbnail('medium');
											} ?>
										</figure>
										<aside class="leader-info">
											<h3><?php the_title(); ?></h3>
											<h6><?php if( get_field('team_title') ) { the_field('team_title'); } ?>
												<?php if( get_field('region') ) { echo ' | ' . get_field('region'); } ?>
											</h6>
											<?php the_content(); ?>
										</aside>
									</div>
									
								<?php endwhile;endif;wp_reset_postdata(); ?>

							</div> <!-- leadership -->


							












							<?php endif; ?>











								
							</div>
						<?php $count++;endwhile;endif;wp_reset_postdata(); ?>

					</div>
				</div>

			</div>

		</section>

		<?php get_template_part('template-parts/content', 'pre-footer-featured-ctas'); ?>

	</main><!-- #main -->

<?php
get_footer();

