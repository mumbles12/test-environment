<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 30_Lines_Properties
 */

$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
get_header(); ?>

<?php
	while ( have_posts() ) : the_post();
	if( has_post_thumbnail() && has_post_thumbnail() != null ) : ?>

	<header class="hero is-single-prop has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
	</header>
	<?php endif; ?>

	<main id="main" class="row padded-y" role="main">
		<section class="entry-content medium-8 columns">

			<?php get_template_part( 'template-parts/content', 'page' ); ?>

		</section>

		<?php get_sidebar(); ?>

	</main><!-- #main -->

<?php
endwhile; // End of the loop.
get_footer();
