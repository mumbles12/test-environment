<?php
/**
 * Template Name: Contact Page
 *
 * @package 30_Lines_Properties
 */
$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
get_header(); ?>
	
	<div id="angular-page-template-wrapper">

		<header class="hero is-single-prop has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
		
		</header>

		<main id="main" role="main" ng-controller="mainCtrl">

			<section class="is-left-navigation-content">

				<div class="clearfix small-collapse">

					<div class="large-4 columns">
						<ul class="vertical tabs" data-tabs id="example-tabs">
							<?php 
							$count = 0;
							if ( have_rows('tab_navigation_content') ) : while ( have_rows('tab_navigation_content') ) : the_row(); 
								$tabTextNormal = get_sub_field('tab_text');
								$slug = sanitize_title(get_sub_field('tab_text'));
								$isActive = $count == 0 ? 'is-active' : '';
								echo "<li class='tabs-title {$isActive}'><a data-tab-number='{$count}' href='#{$slug}' aria-selected='true'>{$tabTextNormal}</a></li>";
								$count++;
							endwhile;endif;wp_reset_postdata(); ?>
						</ul>
					</div>
					<div class="large-8 columns">
						<div class="tabs-content contact-page-tab-content" data-tabs-content="example-tabs">

							<?php 
							$count = 0;
							if ( have_rows('tab_navigation_content') ) : while ( have_rows('tab_navigation_content') ) : the_row(); 
								$tabTextNormal = get_sub_field('tab_text');
								$slug = sanitize_title(get_sub_field('tab_text'));
								$isActive = $count == 0 ? 'is-active is-overview' : ''; ?>
								<div class="tabs-panel <?php echo $isActive; ?>" id="<?php echo $slug; ?>">

									<section class="tab-data-wrapper">
										<div class="data-content">
											<?php if ( get_sub_field('tab_content_title') ) : ?>
												<h3><?php the_sub_field('tab_content_title'); ?></h3>
											<?php endif; ?>

											<?php the_sub_field('tab_content'); ?>
										</div>
									</section>

									<?php if ( get_sub_field('lat') && get_sub_field('long') ) : ?>
										<aside class="tab-map">
											<div 
												id="map-canvas-<?php echo $slug; ?>" 
												style="height: 450px; display:block;"
												data-lat="<?php echo get_sub_field('lat'); ?>"
												data-long="<?php echo get_sub_field('long'); ?>"
												data-map-number="<?php echo $count; ?>"
											></div>
										</aside>
									<?php endif; ?>

									<footer class="tab-form" id="contactTabForm">
										<?php the_sub_field('grav_shortcode'); ?>
									</footer>
								</div>

							<?php $count++; endwhile; endif; wp_reset_postdata(); ?>
						</div>
					</div>

				</div>

			</section>

		</main><!-- #main -->
	</div>

    <script>
    	var styledMapType=new google.maps.StyledMapType([
		    {
		        "featureType": "administrative",
		        "elementType": "labels.text.fill",
		        "stylers": [
		            {
		                "color": "#444444"
		            }
		        ]
		    },
		    {
		        "featureType": "landscape",
		        "elementType": "all",
		        "stylers": [
		            {
		                "color": "#f2f2f2"
		            }
		        ]
		    },
		    {
		        "featureType": "poi",
		        "elementType": "all",
		        "stylers": [
		            {
		                "visibility": "off"
		            }
		        ]
		    },
		    {
		        "featureType": "road",
		        "elementType": "all",
		        "stylers": [
		            {
		                "saturation": -100
		            },
		            {
		                "lightness": 45
		            },
		            {
		                "color": "#ca956d"
		            }
		        ]
		    },
		    {
		        "featureType": "road",
		        "elementType": "labels.text",
		        "stylers": [
		            {
		                "visibility": "simplified"
		            },
		            {
		                "color": "#000000"
		            }
		        ]
		    },
		    {
		        "featureType": "road.highway",
		        "elementType": "all",
		        "stylers": [
		            {
		                "visibility": "simplified"
		            }
		        ]
		    },
		    {
		        "featureType": "road.arterial",
		        "elementType": "labels.icon",
		        "stylers": [
		            {
		                "visibility": "off"
		            }
		        ]
		    },
		    {
		        "featureType": "transit",
		        "elementType": "all",
		        "stylers": [
		            {
		                "visibility": "off"
		            }
		        ]
		    },
		    {
		        "featureType": "water",
		        "elementType": "all",
		        "stylers": [
		            {
		                "color": "#3f8aad"
		            },
		            {
		                "visibility": "on"
		            }
		        ]
		    }
		], {name: 'Styled Map'});

    	(function($) {
	    	$(document).ready(function() {
				var maps = $('div[id^="map-canvas-"]');
				for(var i  =0; i < maps.length; ++i ){
					var lat = maps[i].getAttribute('data-lat');
					var long = maps[i].getAttribute('data-long');
					initMap(maps[i], lat, long, i);
				}
	    	});
			function initMap(mapEl, lat, long, mapNumber) {
				var office = {lat: parseFloat(lat), lng: parseFloat(long)};
				var map = new google.maps.Map(mapEl, { 
					zoom: 16, 
					center: office,
					mapTypeControlOptions: {
			            mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
		          	}
				});
				var marker = new google.maps.Marker({
					position: office,
					map: map
				});
				var center = map.getCenter();

				map.mapTypes.set('styled_map', styledMapType);
		        map.setMapTypeId('styled_map');

				$('.tabs-title a[data-tab-number="'+mapNumber+'"]')
					.on('click', function() { // tab click handler, force to resize map
						setTimeout(function() { // needs short timeout to wait for tab content to fully load
							google.maps.event.trigger(map, 'resize');
							map.setCenter(center);
						}, 500);
					});
			}
    	})(jQuery);
    </script>

<?php
get_footer();

