<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 30_Lines_Properties
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php if( is_user_logged_in()) : ?>
<style type="text/css">
@media screen and (min-width: 40em) {
	.main-nav.nav-down {
	    top: 32px !important;
	}
}
</style>
<?php endif; ?>

<?php wp_head(); ?>
<script> (function(d) { var config = { kitId: 'vyf3qws', scriptTimeout: 3000, async: true }, h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s) })(document); </script>
</head>

<body <?php body_class(); ?>>


<header class="main-header" id="page-top" role="header">
	<nav class="main-nav nav-down" role="navigation">
		<div class="row menu-container">

			<span id="navToggle" class="toggle fa show-for-small-only"></span>

			<ul class="site-title">
				<li>
					<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php if( get_field('site_header_logo', 'option')) { 
							$image = get_field('site_header_logo', 'option');
							$size = 'medium';
							echo wp_get_attachment_image( $image, $size, array( "class" => "header-logo" ) );
						} else {
							echo '<img src="'.get_bloginfo('stylesheet_directory').'/img/Roscoe_Logo-Mark_New-Green.png" class="header-logo" alt="'.get_bloginfo('title').'" />';
						} ?>
					</a>
				</li>
			</ul>

			<div class="nav-search">
				<?php get_search_form(); ?>
			</div>

			<?php
				$primary = array(
					'theme_location' => 'primary', 
					'menu_id' => 'primary-menu',
					'menu_class' => 'menu',
					'container'=>false
				);
				wp_nav_menu( $primary );
			?>

		</div> <!-- Row End Container -->
	</nav>

</header><!-- #masthead -->

<div id="content" class="site-content">
