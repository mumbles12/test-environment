<?php
/**
 * Template Name: Careers
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 30_Lines_Properties
 */

$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
get_header(); ?>

<?php
	while ( have_posts() ) : the_post();
	if( has_post_thumbnail() && has_post_thumbnail() != null ) : ?>

	<header class="hero is-single-prop has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
	</header>
	<?php endif; ?>

	<main id="main" class="padded-y careers-main-container" role="main">

		<section class="entry-content row">

			<div class="small-12 columns">
				<?php the_content(); ?>
			</div>

		</section>


		<?php if( have_rows('opportunity_cards') ): ?>

			<section class="careers-cta-links clearfix">

				<div class="row section-title-row">
					<div class="column small-12">
						<h2 class="padded-y text-left accent-type">View available positions</h2>
					</div>
				</div>

			<?php while( have_rows('opportunity_cards') ): the_row(); 

				// vars
				$image = get_sub_field('photo');
				$title = get_sub_field('title');
				$content = get_sub_field('excerpt');
				$button = get_sub_field('button_text');
				$link = get_sub_field('button_link');

				?>

				<div class="is-career-link">
					<figure>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</figure>
					<div class="cta-details">
						<h4><?php echo $title; ?></h4>
						<?php echo $content; ?>
						<a href="<?php echo $link; ?>" class="button"><?php echo $button; ?></a>
					</div>
				</div>

			<?php endwhile; ?>

			</section>

		<?php endif; ?>

		<?php if( have_rows('icon_cards') ): ?>

			<section class="career-cards padded-y clearfix">

				<div class="row padded-y">
					<?php the_field('icons_section_header'); ?>
				</div>

				<div class="career-card-wrapper clearfix" id="careerCards">

			<?php while( have_rows('icon_cards') ): the_row(); 

				// vars
				$image = get_sub_field('icon');
				$title = get_sub_field('title');
				$content = get_sub_field('excerpt');

				?>

				<div class="is-card">
					<div class="icon-card-wrapper">
						<figure>
							<?php echo $image; ?>
						</figure>
						<h4><?php echo $title; ?></h4>
						<p class="is-excerpt"><?php echo $content; ?></p>
					</div>
				</div>

			<?php endwhile; ?>

				</div>
			</section>

		<?php endif; ?>






		<?php if( have_rows('testimonials') ): ?>
			<section class="clearfix large-collapse testimonial-wrapper" data-equalizer>

				<h3 class="text-center padded-y"><?php the_field('testimonials_header'); ?></h3>

				<aside class="testmonial-video large-8 large-offset-2 xlarge-6 xlarge-offset-0 columns" data-equalizer-watch>

					<div class="video-wrapper">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/zUfMg9ehvfU" frameborder="0" allowfullscreen></iframe>
					</div>

				</aside>

				<section class="current-employees large-12 end xlarge-6 columns" data-equalizer-watch>

					<div class="slides-wrapper-2">

						<?php while( have_rows('testimonials') ): the_row(); 
							// vars
							$image = get_sub_field('photo');
							$name = get_sub_field('name');
							$position = get_sub_field('position');
							$location = get_sub_field('location');
							$quote = get_sub_field('quote'); ?>

							<div class="employee-block">
								<figure>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
								</figure>
								<div class="employee-details">
									<div class="is-details-wrapper">
										<h3><?php echo $name; ?></h3>
										<h6><?php echo $position; ?> <span>|</span> <?php echo $location->post_title; ?></h6>
										<?php if(get_sub_field('tenure')) { echo '<span class="tenure">' . get_sub_field('tenure') .'</span>'; } ?>
										<?php echo $quote; ?>
									</div>
								</div>
							</div>

						<?php endwhile; ?>

					</div>
				</section>

			</section>
		<?php endif; ?>


		<?php if( have_rows('career_logos') ): ?>

		<section class="cta-tour has-bg-img">
			<div class="green-trans-wrapper trans-wrapper">
				<div class="row">
					<div class="small-12 columns cta-awards">

			<?php while( have_rows('career_logos') ): the_row(); 

				// vars
				$image = get_sub_field('logo');
				$link = get_sub_field('link');

				?>

				<?php if( $link ): ?>
					<a href="<?php echo $link; ?>">
				<?php endif; ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />

				<?php if( $link ): ?>
					</a>
				<?php endif; ?>

			<?php endwhile; ?>

					</div>
				</div>
			</div>
		</section>

		<?php endif; ?>



	</main><!-- #main -->

<?php
endwhile; // End of the loop.
get_footer();
