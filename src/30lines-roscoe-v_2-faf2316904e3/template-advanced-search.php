<?php
/**
 * Template Name: Advanced Search
 *
 *
 * @package 30_Lines_Properties
 */

get_header(); 

$regions=get_terms('state');

$regions_to_neighborhoods=[];

foreach ($regions as $region) {
	$regions_to_neighborhoods[ $region->slug ] = get_posts(array(
		'post_type' => 'neighborhoods', 
		'post_status' => 'publish', 
		'nopaging' => true, 
		'posts_per_page' => -1,

		'tax_query' => array(array(
			'taxonomy' => 'state',
			'field' => 'term_id',
			'terms' => array( $region->term_id )
		)),
			
		'orderby' => 'title',
		'order' => 'ASC',
	));
}

$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
?>

<script>
	// angular.module('filters.stringUtils', [])

	// .filter('removeSpaces', [function() {
	//     return function(string) {
	//         if (!angular.isString(string)) {
	//             return string;
	//         }
	//         return string.replace(/[\s]/g, '');
	//     };
	// }]);
</script>

	<header class="home-page-hero hero has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
		
		<h1 class="screen-reader-text"><?php the_title(); ?></h1>

	</header>
	<main id="main" role="main">
		<div id="angular-rent-press-wrapper-1">
			<rent-press-properties init-query-args='{"properties_within_radius": 45}' use-url-parameters="true">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
					<section class="community-filters">
						<!-- 					
							<header class="row">
								<div class="large-9 columns end large-offset-3">
									<h2 class="is-title">Communities as eclectic as Austin</h2>
								</div>	
							</header>
						-->

						<section class="row filter-section">
							<aside class="large-2 columns">
								<h4 class="is-tagline">Find the perfect fit</h4>
							</aside>

							<div class="-filter-controls large-10 columns">
								<form ng-submit="form_submit()" name="prop_filters" class="advanced-search" >
									<input type="hidden" ng-model="property_filter.properties_with_available_units" ng-value="true"> 

									<div class="row medium-collapse">
										<div 
											ng-controller="SuperSearch" 
											style="position: relative; width: 100%; padding-bottom: 15px;" 
											ng-mouseleave="show_locations_dropdown=false;isSearching=false;"
											ng-init="show_locations_dropdown=false; property_filter.anywhere='';"

										>
											<!-- <div  style="white-space: nowrap; overflow: hidden; position: relative;" >
												
												<div style="display: block; width: 100%; border: 1px solid #7ec78b; padding: 0px 15px; padding-top: 3px; height: auto; background-color: #18382a; font-size:0.85rem !important; padding: .5rem;  margin-bottom: 15px;" ng-click="show_locations_dropdown=true; focusToInput();" ng-hide="(property_filter.super_search.length == 0 || property_filter.super_search == undefined) && show_locations_dropdown==true">
												
													<span ng-show="(property_filter.super_search.length == 0 || property_filter.super_search == undefined)" style="text-transform: uppercase;">Search for a city, or state, or zip</span>

													<span>
														<span style="display: inline;" ng-if="property_filter.super_search.isA == 'neighborhood'">
															(City: {{ property_filter.super_search.wp_query_var.value }})
														</span>

														<span style="display: inline;" ng-if="property_filter.super_search.isA == 'state'">
															(State: {{ property_filter.super_search.wp_query_var.value }})
														</span>

														<span style="display: inline;" ng-if="property_filter.super_search.isA == 'zip'">
															(Zipcode: {{ property_filter.super_search.wp_query_var.value }})
														</span>

														<span style="display: inline;" ng-if="property_filter.super_search.isA == 'property'">
															(Property: {{ property_filter.super_search.forFiltering }})
														</span>
													</span>
												</div>
											</div> -->
											<input type="search" ng-model="property_filter.anywhere" placeholder="Search for a city, or state, or zip"
												ng-keydown="inputKeydown($event)"
												ng-focus="show_locations_dropdown=true; isSearching=true;"
												ng-blur="isSearching=false"
												ng-change="show_locations_dropdown=true;"
												style="background-color: #18382a !important; margin-bottom: 0px;"
												ng-click="show_locations_dropdown=true;focusToInput();"
												tabindex="1" 
												>
										
											<div class="search-dropdown-wrapper" ng-show="property_filter.anywhere != '' && show_locations_dropdown==true" 
												ng-mouseleave="show_locations_dropdown=false;"
												tabindex="1"> 

												<ul>
													<li ng-repeat="opt in filteredOptions = (super_search_options | orderBy:'forOrdering' | filter:{forFiltering:property_filter.anywhere })">

														<label style="float: none; width: 100%;" ng-class="{'checked': opt.wp_query_var.value && property_filter.anywhere == opt.wp_query_var.value}" ng-click="input_changed(opt, true);" tabindex="{{ $index+2 }}" ng-keydown="itemKeyed($event, opt)">
															<input style="display: none;" type="radio" ng-model="property_filter.super_search" ng-value="opt" 
															ng-change="input_changed(opt, checked);"> 

															<span ng-bind-html="opt.displayText"></span>
														</label>
													</li>
												</ul>
											</div>
										</div>
									</div>
	
									<div class="row medium-collapse">

										<section class="medium-6 large-3 columns is-btn-group">
											<div class="btn-group-wrapper">
												<header>
													<h6><span class="fa fa-bed"></span> Bedrooms</h6>
												</header>
												<div class="button-group">

													<input id="studio" type="checkbox" checklist-model="property_filter.properties_beds" checklist-value="'0'">
													<label for="studio" class="button">Studio</label>

													<input id="1bed" type="checkbox" checklist-model="property_filter.properties_beds" checklist-value="'1'">
													<label for="1bed" class="button">1</label>

													<input id="2bed" type="checkbox" checklist-model="property_filter.properties_beds" checklist-value="'2'">
													<label for="2bed" class="button">2</label>

													<input id="3bed" type="checkbox" ng-model="property_filter.properties_min_beds" ng-true-value="'3'">
													<label for="3bed" class="button" ng-click="property_filter.properties_beds=[];">3+</label>

												</div>
											</div>

											<div class="btn-group-wrapper">
												<header>
													<h6><span class="fa fa-bath"></span> Bathrooms</h6>
												</header>
												<div class="button-group">

													<input id="any" type="checkbox" ng-checked="property_filter.properties_baths.length==0 || property_filter.properties_baths == undefined">
													<label for="any" class="button" ng-click="property_filter.properties_baths=[];" >Any</label>

													<input id="1bath" type="checkbox" checklist-model="property_filter.properties_baths" checklist-value="'1'">
													<label for="1bath" class="button">1</label>

													<input id="2bath" type="checkbox" checklist-model="property_filter.properties_baths" checklist-value="'2'">
													<label for="2bath" class="button">2</label>

													<input id="3bath" type="checkbox" checklist-model="property_filter.properties_baths" checklist-value="'3'">
													<label for="3bath" class="button">3</label>

												</div>
											</div>

										</section><!-- is-btn-group -->

										<section class="medium-6 columns is-slider">

											<header>
												<h6><span class="fa fa-usd"></span> Rent</h6>
											</header>

											<div class="row medium-collapse" ng-controller="slidersDefulatValues">

												<div class="clearfix slider-block small-collapse">

													<!-- <div class="slider-values small-3 medium-2 columns">
														<span>$<input type="number" ng-model="property_filter.properties_min_rent" id="sliderOutput1" value="1050"></span>
													</div> -->

													<section class="small-11 medium-11 columns">

													<!-- 	<div class="slider" data-slider data-binding="true" data-double-sided="true" data-start="800" data-initial-start="1050" data-end="1800" data-initial-end="1400">
															<span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="sliderOutput1"></span>
															<span class="slider-fill" data-slider-fill></span>
															<span class="slider-handle" data-slider-handle role="slider" tabindex="1" aria-controls="sliderOutput2"></span>
														</div> -->

														<rzslider
															class="custom-slider"
														    rz-slider-model="property_filter.properties_min_rent"
														    rz-slider-high="property_filter.properties_max_rent"
														    rz-slider-options='rentRangeSliderOptions'></rzslider>

													</section>

													<!-- <div class="slider-values small-3 medium-2 columns">
														<span>$<input type="number" ng-model="property_filter.properties_max_rent" id="sliderOutput2" value="1400"></span>
													</div>  -->
												</div> <!-- slider-block -->

												<header>
													<h6><span class="fa fa-usd"></span> Sq Ft</h6>
												</header>

												<div class="clearfix slider-block small-collapse">
													<!-- <div class="slider-values small-3 medium-2 columns">
														<span>*<input type="number" ng-model="property_filter.properties_min_sqft" id="sliderOutput3"></span>
													</div> -->

													<section class="small-11 medium-11 columns">

														<rzslider
															class="custom-slider"
														    rz-slider-model="property_filter.properties_min_sqft"
														    rz-slider-high="property_filter.properties_max_sqft"
														    rz-slider-options='sqftRangeSliderOptions'></rzslider>
														    
													</section>

													<!-- <div class="slider-values small-3 medium-2 columns">
														<span>*<input type="number" ng-model="property_filter.properties_max_sqft" id="sliderOutput4"></span>
													</div> -->
												</div> <!-- slider-block -->



											</div>	<!-- sliders parent section -->

										</section>

										<aside class="large-3 columns">

											<header>
												<h6><span class="fa fa-calendar"></span> Move-In Date</h6>
											</header>

											<input type="date" placeholder="" ng-model="property_filter.properties_available_by" >
											
											<header>
												<h6><span class="fa fa-refresh"></span> Submit</h6>
											</header>
											<button id="formUpdate" class="button update-btn">Update Filters</button>

										</aside>
									</div>
								</form>
							</div>

						</section>

					</section> <!-- community-filters -->

					<section class="view-btns">
						<nav class="view-toggles">
							<span id="viewGrid" class="fa fa-th"></span>
							<span id="viewRow" class="fa fa-th-list show-for-medium"></span>
							<span id="viewMap" class="fa fa-map-marker is-active"></span>
						</nav>
					</section>

					<section id="gridContainer" class="grid-results is-grid-v">

						<p ng-if="gathering_from_api == true" class="text-center">Loading...</p>

						<p ng-show="property_ids.length == 0" style="text-align: center;">None Founded</p>

						<div class="is-property animated fadeIn" ng-repeat="property in filteredAndOrderedProperties track by property.ID">
						
								<figure class="is-photo">
									<div class="list-special" ng-if="property.has_special">
										<p title="{{ property.special_description }}">{{ property.special_title }}</p>
									</div>
									<a ng-href="{{ property.permalink }}">
										<img ng-src="{{ property.image }}" alt="">
									</a>
								</figure>
								<section class="is-details">
									<div class="details-wrapper">
										<a ng-href="{{ property.permalink }}">
											<h4>{{ property._title }}</h4>
										</a>
										
										<!-- <h6>Explore <span>Community</span></h6> -->
										<p>{{ property.meta_data.propCity }}, {{ property.meta_data.propState }}</p>

										<span class="is-phone for-desktop" ng-if="property.meta_data.propPhoneNumber">
											<a ng-href="tel:{{ property.meta_data.propPhoneNumber }}">
												<i class="fa fa-mobile"></i> {{ property.meta_data.propPhoneNumber }}
											</a>
										</span>

										<span class="is-phone for-mobile" ng-if="property.meta_data.propPhoneNumber">
											<a ng-href="tel:{{ property.meta_data.propPhoneNumber }}">
												<i class="fa fa-mobile"></i>
											</a>
										</span>

										<div>
											<a ng-href="https://maps.google.com/?saddr=My%20Location&daddr={{ property.meta_data.propAddress + ' ' + property.meta_data.propCity + ', ' + property.meta_data.propState + property.meta_data.propZip }}" class="is-map-link"><i class="fa fa-map-marker"></i></a>
										</div>
										
										<div class="grid-special" ng-if="property.has_special">
											<p title="{{ property.special_description }}">{{ property.special_title }}</p>
										</div>
									</div>
								</section>
							
						</div>

					</section>

					<script type="text/javascript">
					</script>


					<section class="is-map-search-container is-active" id="mapResults" ng-controller="MapResults">

						<div class="map-img-wrapper clearfix">
							<img src="<?php echo get_template_directory_uri(); ?>/img/map-nostatelines.png" alt="">

							<div class="is-state texas" id="stateTexas" data-state="Texas" ng-click="iWantToViewStatesCities('TX')">

								<div class="state-details">
									<span class="state-name" id="stateName">Texas</span>
									<div class="apart-number map-icon" id="apartNum">
										<span>{{ (filteredProperties | filter:{'meta_data': {'propState': 'TX',},}).length }}</span>
									</div>
								</div>

								<!-- <img src="<?php //echo get_template_directory_uri(); ?>/img/texas.svg" alt=""> -->
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 431 412">
								  <path class="cls-1" d="M219.586,0h-101.9V178.805H0l1.435,8.583s2.317,2.01,7.176,8.583,5.466,4.717,11.482,8.582S35.88,220.288,35.88,220.288a9.441,9.441,0,0,0,7.176,7.152c6.133,1.362,6.066.218,10.047,5.722s4.306,8.583,4.306,8.583l4.306,7.152s-0.626,10.078,4.306,15.735,9.11,11.291,12.917,14.3a54.912,54.912,0,0,0,10.046,5.722l5.741,1.43s9.21,9.65,12.917,10.013a40.167,40.167,0,0,0,7.176,0l7.176-7.152,1.436-2.861,1.435-7.152,4.305-11.443,10.047-1.431,4.305-2.861s4.441,2.619,17.223,2.861,8.83,0.62,12.917,4.292,3.556,4.989,8.611,7.152,8.538,4.069,11.482,11.443,4.268,17.188,8.611,21.457,5.817,8.149,7.176,11.443,8.611,10.014,8.611,10.014l5.741,8.582,10.046,7.152V357.61l1.435,4.292,2.871,10.013a5.951,5.951,0,0,1,4.306,4.291c1.169,3.763,2.072,10.509,4.3,12.874s8.524,4.63,10.047,5.722a54.63,54.63,0,0,0,7.176,4.291c1.672,0.684,3.589-.873,5.74,0s2.37,3.435,7.176,5.722,9.765,0.211,15.788,1.43,5.83-.016,8.611,2.861,4.306,2.861,4.306,2.861l5.74-2.861-2.87-7.152s-4.774-6.616-5.741-8.583,0-8.582,0-8.582l-1.435-5.722-1.435-2.861v-5.722s0.363-.16,1.435-2.86a9.163,9.163,0,0,0,0-5.722,2.063,2.063,0,0,1-2.871-1.431c-0.9-2.367.034-2.282,1.436-2.861a28.955,28.955,0,0,0,4.3-2.86v-5.722l4.306-4.292-4.306-1.43-1.435-4.291,4.306-1.431h4.305l4.306-4.291s1.541-2.36,0-2.861a4.721,4.721,0,0,0-2.87,0l2.87-2.861,2.87,1.431,4.306-1.431,1.435-4.291v-4.291l2.871,1.43,4.305-2.861h4.306L334.4,314.7l-4.306-4.291v-4.291l2.87,2.86h4.306l5.741-1.43s-0.874,1.562,1.435,1.43,7.176,0,7.176,0l4.306,1.431-1.436-4.291,4.306-1.431s11.512-2.276,15.787-8.583,4.647-9.135,5.741-8.582,3.456-.93,4.306-2.861,1.435-7.152,1.435-7.152-2.614-2.587-1.435-4.292,0.958-2.112,2.87-2.86,1.374-3.592,2.871-2.861,2.808-.363,2.87,1.43-2.075,6.043,1.435,5.722a22.993,22.993,0,0,0,7.176-1.431c3.149-1.045,6.217-2.511,8.611-2.86s6.492-1.022,7.176-1.431,3.492-.462,2.871-1.43-0.583-6.172,2.87-10.013,1.435-10.014,1.435-10.014-1.139-9.616,2.871-15.734,4.236-20.136,1.435-22.887-5.741-14.305-5.741-14.305l-4.305-8.583-4.306-4.291,1.435-48.635-2.87-12.874-2.871-1.43a32.721,32.721,0,0,0-4.305,0,32.926,32.926,0,0,1-4.306,0l-2.87-2.861-2.871-1.431-11.481-5.721-5.741-2.861H367.412l-7.176-1.431-2.87,1.431-7.176,1.43a15.307,15.307,0,0,0-2.871,1.431c-1.114.781-2.441,4.012-5.74,4.291s-7.882.277-8.612-1.431-2.748-4.22-4.305-4.291-4.306,1.431-4.306,1.431l-5.741-4.292H314.31v5.722l-5.741-4.291-1.435,1.43-2.871,1.431-2.87-2.861-5.741-2.861-4.306,2.861-4.305,1.43-1.435-4.291L278.43,97.27l-4.306-1.43s-2.845,3.917-5.741,2.861-3.08-3.286-5.741-2.861a37.386,37.386,0,0,1-7.176,0s-4.159-2.864-5.74-2.861-4.306,0-4.306,0-1.133-6.418-4.306-7.152-0.362,1.976-4.305,1.431S226.121,85.84,225.327,84.4s-7.176-5.722-7.176-5.722Z"/>
								</svg>
					
							</div> <!-- is-state -->

							<div class="is-state colorado" id="stateColorado" data-state="Colorado" ng-click="iWantToViewStatesCities('CO')">

								<div class="state-details">
									<span class="state-name" id="stateName">Colorado</span>
									<div class="apart-number map-icon" id="apartNum">
										<!-- <span>{{ filteredProperties.length }}</span> -->
										<span>{{ (filteredProperties | filter:{'meta_data': {'propState': 'CO',},}).length }}</span>
									</div>
								</div>

								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 233 173">
									<path class="cls-1" d="M1,1.2L232,1V172L1,171.8V1.2Z"/>
								</svg>
					
							</div> <!-- is-state -->
						</div> <!-- map-img-wrapper -->


						<aside id="stateFocus" class="state-focus" 
							ng-repeat="(state, cities) in values_of_filters_from_properties.cities_of_state track by $index" 
							ng-class="{'is-active': view_state == state,}"
							data-state="{{ state }}">

							<div class="clearfix state-wrapper">

								<section class="is-state-side">

									
									<div class="state-img-wrapper">

										<img ng-src="<?php echo esc_attr( get_template_directory_uri() ); ?>/img/{{ state }}.png">
										<!-- <img ng-src="http:<?php echo esc_attr( get_template_directory_uri() ); ?>/img/TX.png"> -->
										<ul class="state-locations">
											<li ng-repeat="city in cities" class="city-location" data-city="{{ city }}" ng-click="iWantToViewCity(city);">
												<span class="small-map-icon"></span><strong>{{ city }}</strong>
											</li>
										</ul>
									</div>


								</section> <!-- is-state-side -->

								<aside class="is-list-side">
									
									<accordion>
										<accordion-group ng-repeat="city in cities" heading="{{ city }}" data-city="{{ city }}">
											<p ng-repeat="property in filteredProperties | filter:{'meta_data': {'propCity': city}} track by $index">
												<a href="{{ property.permalink }}">
													{{ property._title }}
												</a>
											</p>
										</accordion-group>
									</accordion>

								</aside>
							</div>

							<div class="back-to-map" id="backToMap" ng-click="exitCitiesView();">
								<span class="map-icon"><span><i class="fa fa-angle-left"></i></span></span>
							</div>
						</aside>
					</section> <!-- mapResults -->

<!-- 					<section class="minimal-cta">
						<div class="row" data-equalizer>
							<header class="medium-5 columns" data-equalizer-watch>
								<h5>Can't <span>Decide?</span></h5>
							</header>
							<aside class="medium-7 columns" data-equalizer-watch>
								<p>Lorem ipsum dolor sit amet, oluptatem hic rem excepturi, <a>nemo mollitia</a> ipsam, adipisci esse optio!</p>
								<?php //the_content(); ?>
							</aside>
						</div>
					</section>
 -->
					<?php // get_template_part( 'template-parts/content', 'subfooter' ); ?>
						
				<?php endwhile; endif; ?>
			</rent-press-properties>
		</div>

		<script type="text/javascript">
			var angularSearchTemplateApp = angular.module('angularSearchTemplateApp', ['rentPress', 'mm.foundation']);

			angularSearchTemplateApp.controller('AdvancedSearchMapStyles', ['$scope', function($scope) {
				$scope.advancedSearchMapStyles = {
		            styles: [{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"saturation":"0"},{"gamma":"2.53"},{"weight":"1.54"},{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"geometry.stroke","stylers":[{"weight":"0.01"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#C5E3BF"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#D1D1B8"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#c691da"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#C6E2FF"}]}]
		        };

			}]);

			angularSearchTemplateApp.controller('SuperSearch', ['$rootScope', '$scope', function($scope, $rootScope) {

				if (typeof $scope.property_filter == 'undefined') {
					$scope.property_filter={
	                    properties_within_distance_from: {},
	                    filteringSuperSearch: _$_URL_GET.filteringSuperSearch,
	                };
				}

				$scope.super_search_options=[];

				angularFilterByValues.states.forEach(function(state) {
					$scope.super_search_options.push({
						isA: 'state',
						wp_query_var: {
							key: 'properties_state',
							value: state.short,	
						},

						forOrdering: state,
						forFiltering: state.long+"("+ state.short +")",
						displayText: '<i class="fa fa-map-marker"></i> Browse Apartments In '+ state.long,
					});
				});

				_.forEach(angularFilterByValues.neighborhoods, function(neighborhood) {
					$scope.super_search_options.push({
						isA: 'neighborhood',
						wp_query_var: {
							key: 'properties_city',
							value: neighborhood.post_title,	
						},

						forOrdering: neighborhood.state+', 0, '+neighborhood.post_title,
						forFiltering: neighborhood.post_title + ", "+ neighborhood.state,
						displayText: '<i class="fa fa-map-marker"></i> Browse Apartments In '+ neighborhood.post_title +', '+neighborhood.state,
					});
				});

				angularFilterByValues.zipcodes_with_cities_and_states.forEach(function(zip) {
					$scope.super_search_options.push({
						isA: 'zip',
						wp_query_var: {
							key: 'properties_zipcode',
							value: zip.zip,
						},

						forOrdering: zip.state+', 0, '+zip.city+', '+zip.zip,
						forFiltering: zip.cityStateZip,
						displayText: '<i class="fa fa-map-marker"></i> Browse Apartments Near Zip | '+ zip.zipState +'</a>',
					});
				});

				_.forEach(angularFilterByValues.properties, function(p) {
					$scope.super_search_options.push({
						isA: 'property',
						url: encodeURI(p.guid),

						forOrdering: p.state+', 0, '+p.city+', 0, '+p.post_title,
						//forFiltering: p.city+' '+ p.post_title,
						forFiltering: p.searchTerms,
						displayText: '<i class="fa fa-building-o"></i>&nbsp;'+ p.post_title,
					});
				});
			
				$scope.input_changed=function(optValue, checked) {
					if (checked == true && typeof optValue.url != 'undefined') {
						window.open(optValue.url, '_self');
					} 
					else {
						if (! Array.isArray($scope.property_filter.super_search) && $scope.property_filter.super_search != undefined && typeof $scope.property_filter.super_search.url != 'undefined') {
							window.open($scope.property_filter.super_search.url, '_self');
						}						
					}

					if (typeof optValue.wp_query_var != 'undefined') {
						$scope.property_filter.anywhere=optValue.wp_query_var.value;
						$scope.property_filter.super_search={};

						//document.querySelector('input[ng-model="filteringSuperSearch"]').val(optValue.wp_query_var.value)
					}
				};

				$scope.super_search_item_click=function(ssi) {
				};

				$scope.itemKeyed = function(e, optValue) {
					var currentItem=document.querySelector('.search-dropdown-wrapper ul li label:focus').parentElement;

					if (e.keyCode === 40 && currentItem.nextElementSibling) {
						var next=currentItem.nextElementSibling.querySelector('label');
							
						if (next) {
							next.focus();
						}

						e.preventDefault();
					}
					else if (e.keyCode === 38 && currentItem.previousElementSibling) {
						var pass=currentItem.previousElementSibling.querySelector('label');

						if (pass) {
							pass.focus();
						}

						e.preventDefault();
					}
					else if (e.keyCode == 13) {

						if (typeof optValue.url != 'undefined') {
							window.open(optValue.url, '_self');
						} 
						else {
							if (! Array.isArray($scope.property_filter.super_search) && $scope.property_filter.super_search != undefined && typeof $scope.property_filter.super_search.url != 'undefined') {
								window.open($scope.property_filter.super_search.url, '_self');
							}						
						}

						if (typeof optValue.wp_query_var != 'undefined') {
							$scope.property_filter.anywhere=optValue.wp_query_var.value;
							$scope.property_filter.super_search={};

							//document.querySelector('input[ng-model="filteringSuperSearch"]').val(optValue.wp_query_var.value)
						}
						
						$scope.show_locations_dropdown=false;
						$rootScope.form_submit();
					}
				}

				$scope.inputKeydown=function(e) {
					if (e.keyCode === 40) {
						document.querySelector('.search-dropdown-wrapper ul li label').focus();
						e.preventDefault();
					}

				}

				$scope.focusToInput=function() {
					$scope.isSearching=true;
					//document.querySelector('input[ng-model="filteringSuperSearch"]').focus();
				};

				/*if (typeof _$_URL_GET.super_search != 'undefined' && Array.isArray(_$_URL_GET.super_search)) {

					_.forEach(_$_URL_GET.super_search, function(ssi_value, ssi_key) {
						if (Array.isArray(ssi_value)) {
							ssi_value.forEach(function(val) {
								$scope.super_search_options.forEach(function(ssi) {
									if (typeof ssi.wp_query_var != 'undefined') {
										if (ssi_key == ssi.wp_query_var.key && val == ssi.wp_query_var.value) {
											if (! Array.isArray($scope.property_filter.super_search)) {
												$scope.property_filter.super_search=[];
											}

											if (_$_URL_GET.super_search.length == 1) {
												$scope.property_filter.super_search=ssi;
											}
											else {
												$scope.property_filter.super_search.push(ssi);
											}
										}
									}
									
								});
							});
						}
						else {
							$scope.super_search_options.forEach(function(ssi) {
								if (typeof ssi.wp_query_var != 'undefined') {
									if (ssi_key == ssi.wp_query_var.key && ssi_value == ssi.wp_query_var.value) {
										if (! Array.isArray($scope.property_filter.super_search)) {
											$scope.property_filter.super_search=[];
										}

										if (_$_URL_GET.super_search.length == 1) {
											$scope.property_filter.super_search=ssi;
										}
										else {
											$scope.property_filter.super_search.push(ssi);
										}
									}
								}
								
							});
						}
					});				
				}
				else if (typeof _$_URL_GET.super_search != 'undefined' && typeof _$_URL_GET.super_search.key != 'undefined') {
					console.log('HELLO WORLD');

					$scope.super_search_options.forEach(function(ssi) {
						if (typeof  ssi.wp_query_var != 'undefined' && typeof  ssi.wp_query_var.key != 'undefined') {
							if (_$_URL_GET.super_search.key == ssi.wp_query_var.key && _$_URL_GET.super_search.value == ssi.wp_query_var.value) {
								if (! Array.isArray($scope.property_filter.super_search)) {
									$scope.property_filter.super_search=[];
								}


								$scope.property_filter.super_search=ssi;
								//$scope.property_filter.filteringSuperSearch=ssi.wp_query_var.value;
							}
							
						}

					});
				}*/
			}]);

			angularSearchTemplateApp.controller('slidersDefulatValues', ['$rootScope', '$scope', function($scope, $rootScope) {
					
				if (typeof $scope.property_filter == 'undefined') {
	                $scope.property_filter={
	                    properties_within_distance_from: {},
	                    super_search: [],
	                };
				}
                
				$rootScope.defaultRentRangeValues($scope.property_filter);

				$rootScope.defaultSqftRangeValues($scope.property_filter);

				console.log($scope.property_filter);
			}]);

			angularSearchTemplateApp.controller('MapResults',  ['$scope', function($scope) {

				$scope.view_state="";

				$scope.iWantToViewStatesCities=function(shortState) {
					$scope.view_state=shortState;
				}

				$scope.exitCitiesView=function() {
					$scope.view_state="";
				}

				$scope.view_city="";

				$scope.iWantToViewCity=function(city) {
					$scope.view_city=city;

					var ele_accord=document.querySelector("li[data-city='"+ city +"']");

					console.log(ele_accord);
					console.log(angular.element(ele_accord));

					angular.element(ele_accord).addClass('was-clicked');
				}
				
			}]);

			angular.bootstrap(
				document.getElementById('angular-rent-press-wrapper-1'), 
				['angularSearchTemplateApp']
			);
		</script>
	</main>

<?php
get_footer();
