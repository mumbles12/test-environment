<?php
/**
 * Template Name: Full Width - No Side Bar
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 30_Lines_Properties
 */

$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
get_header(); ?>

<?php
	while ( have_posts() ) : the_post();
	if( has_post_thumbnail() ) : ?>

	<header class="hero is-single-prop has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
	</header>
	<?php endif; ?>

	<main id="main" class="row padded-y" role="main">
		<section class="entry-content small-12 columns">

			<?php get_template_part( 'template-parts/content', 'page' ); ?>

		</section>

	</main><!-- #main -->

<?php
endwhile; // End of the loop.
get_footer();
