<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<span class="fa fa-search"></span>
    <label>
        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
        <input type="search" class="search-field"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </label>

    <input type="submit" class="search-submit"
        value="" />
</form>