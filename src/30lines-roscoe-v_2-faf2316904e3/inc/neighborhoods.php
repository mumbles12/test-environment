<?php
/**
 * Neighborhoods
 */

// Register Custom Post Type
function neighborhood_post_type() {

	$labels = array(
		'name'                  => _x( 'Neighborhoods', 'Post Type General Name', 'thirty_lines_properties' ),
		'singular_name'         => _x( 'Neighborhood', 'Post Type Singular Name', 'thirty_lines_properties' ),
		'menu_name'             => __( 'Neighborhoods', 'thirty_lines_properties' ),
		'name_admin_bar'        => __( 'Neighborhoods', 'thirty_lines_properties' ),
		'archives'              => __( 'Neighborhood Archives', 'thirty_lines_properties' ),
		'parent_item_colon'     => __( 'Parent Neighborhood:', 'thirty_lines_properties' ),
		'all_items'             => __( 'All Neighborhoods', 'thirty_lines_properties' ),
		'add_new_item'          => __( 'Add New Neighborhood', 'thirty_lines_properties' ),
		'add_new'               => __( 'Add New', 'thirty_lines_properties' ),
		'new_item'              => __( 'New Neighborhood', 'thirty_lines_properties' ),
		'edit_item'             => __( 'Edit Neighborhood', 'thirty_lines_properties' ),
		'update_item'           => __( 'Update Neighborhood', 'thirty_lines_properties' ),
		'view_item'             => __( 'View Neighborhood', 'thirty_lines_properties' ),
		'search_items'          => __( 'Search Neighborhood', 'thirty_lines_properties' ),
		'not_found'             => __( 'Not found', 'thirty_lines_properties' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'thirty_lines_properties' ),
		'featured_image'        => __( 'Featured Image', 'thirty_lines_properties' ),
		'set_featured_image'    => __( 'Set featured image', 'thirty_lines_properties' ),
		'remove_featured_image' => __( 'Remove featured image', 'thirty_lines_properties' ),
		'use_featured_image'    => __( 'Use as featured image', 'thirty_lines_properties' ),
		'insert_into_item'      => __( 'Insert into item', 'thirty_lines_properties' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'thirty_lines_properties' ),
		'items_list'            => __( 'Items list', 'thirty_lines_properties' ),
		'items_list_navigation' => __( 'Items list navigation', 'thirty_lines_properties' ),
		'filter_items_list'     => __( 'Filter items list', 'thirty_lines_properties' ),
	);
	$args = array(
		'label'                 => __( 'Neighborhood', 'thirty_lines_properties' ),
		'description'           => __( 'Available neighborhoods', 'thirty_lines_properties' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'neighborhoods', $args );

}
add_action( 'init', 'neighborhood_post_type', 0 );


// Register Custom Taxonomy
function state_taxonomy() {

	$labels = array(
		'name'                       => _x( 'States', 'Taxonomy General Name', 'thirty_lines_properties' ),
		'singular_name'              => _x( 'State', 'Taxonomy Singular Name', 'thirty_lines_properties' ),
		'menu_name'                  => __( 'States', 'thirty_lines_properties' ),
		'all_items'                  => __( 'All States', 'thirty_lines_properties' ),
		'parent_item'                => __( 'Parent State', 'thirty_lines_properties' ),
		'parent_item_colon'          => __( 'Parent State:', 'thirty_lines_properties' ),
		'new_item_name'              => __( 'New State Name', 'thirty_lines_properties' ),
		'add_new_item'               => __( 'Add New State', 'thirty_lines_properties' ),
		'edit_item'                  => __( 'Edit State', 'thirty_lines_properties' ),
		'update_item'                => __( 'Update State', 'thirty_lines_properties' ),
		'view_item'                  => __( 'View State', 'thirty_lines_properties' ),
		'separate_items_with_commas' => __( 'Separate states with commas', 'thirty_lines_properties' ),
		'add_or_remove_items'        => __( 'Add or remove states', 'thirty_lines_properties' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'thirty_lines_properties' ),
		'popular_items'              => __( 'Popular states', 'thirty_lines_properties' ),
		'search_items'               => __( 'Search States', 'thirty_lines_properties' ),
		'not_found'                  => __( 'Not Found', 'thirty_lines_properties' ),
		'no_terms'                   => __( 'No states', 'thirty_lines_properties' ),
		'items_list'                 => __( 'Items list', 'thirty_lines_properties' ),
		'items_list_navigation'      => __( 'Items list navigation', 'thirty_lines_properties' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'state', array( 'neighborhoods' ), $args );
}
add_action( 'init', 'state_taxonomy', 0 );