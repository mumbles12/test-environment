<?php
// Register Custom Post Type
function team() {

	$labels = array(
		'name'                  => _x( 'Team Members', 'Post Type General Name', 'thirty_lines_properties' ),
		'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'thirty_lines_properties' ),
		'menu_name'             => __( 'Team', 'thirty_lines_properties' ),
		'name_admin_bar'        => __( 'Team', 'thirty_lines_properties' ),
		'archives'              => __( 'Team Archives', 'thirty_lines_properties' ),
		'attributes'            => __( 'Team Attributes', 'thirty_lines_properties' ),
		'parent_item_colon'     => __( 'Parent Item:', 'thirty_lines_properties' ),
		'all_items'             => __( 'All Team Members', 'thirty_lines_properties' ),
		'add_new_item'          => __( 'Add New Team Member', 'thirty_lines_properties' ),
		'add_new'               => __( 'Add New', 'thirty_lines_properties' ),
		'new_item'              => __( 'New Team Member', 'thirty_lines_properties' ),
		'edit_item'             => __( 'Edit Team Member', 'thirty_lines_properties' ),
		'update_item'           => __( 'Update Team Member', 'thirty_lines_properties' ),
		'view_item'             => __( 'View Team Member', 'thirty_lines_properties' ),
		'view_items'            => __( 'View Team Members', 'thirty_lines_properties' ),
		'search_items'          => __( 'Search Team Members', 'thirty_lines_properties' ),
		'not_found'             => __( 'Not found', 'thirty_lines_properties' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'thirty_lines_properties' ),
		'featured_image'        => __( 'Featured Image', 'thirty_lines_properties' ),
		'set_featured_image'    => __( 'Set featured image', 'thirty_lines_properties' ),
		'remove_featured_image' => __( 'Remove featured image', 'thirty_lines_properties' ),
		'use_featured_image'    => __( 'Use as featured image', 'thirty_lines_properties' ),
		'insert_into_item'      => __( 'Insert into item', 'thirty_lines_properties' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'thirty_lines_properties' ),
		'items_list'            => __( 'Items list', 'thirty_lines_properties' ),
		'items_list_navigation' => __( 'Items list navigation', 'thirty_lines_properties' ),
		'filter_items_list'     => __( 'Filter items list', 'thirty_lines_properties' ),
	);
	$args = array(
		'label'                 => __( 'Team', 'thirty_lines_properties' ),
		'description'           => __( 'Post Type Description', 'thirty_lines_properties' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-id',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'team', 0 );


// Register Custom Taxonomy
function team_tax() {

	$labels = array(
		'name'                       => _x( 'Branches', 'Taxonomy General Name', 'thirty_lines_properties' ),
		'singular_name'              => _x( 'Branch', 'Taxonomy Singular Name', 'thirty_lines_properties' ),
		'menu_name'                  => __( 'Branch', 'thirty_lines_properties' ),
		'all_items'                  => __( 'All Items', 'thirty_lines_properties' ),
		'parent_item'                => __( 'Parent Item', 'thirty_lines_properties' ),
		'parent_item_colon'          => __( 'Parent Item:', 'thirty_lines_properties' ),
		'new_item_name'              => __( 'New Item Name', 'thirty_lines_properties' ),
		'add_new_item'               => __( 'Add New Item', 'thirty_lines_properties' ),
		'edit_item'                  => __( 'Edit Item', 'thirty_lines_properties' ),
		'update_item'                => __( 'Update Item', 'thirty_lines_properties' ),
		'view_item'                  => __( 'View Item', 'thirty_lines_properties' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'thirty_lines_properties' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'thirty_lines_properties' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'thirty_lines_properties' ),
		'popular_items'              => __( 'Popular Items', 'thirty_lines_properties' ),
		'search_items'               => __( 'Search Items', 'thirty_lines_properties' ),
		'not_found'                  => __( 'Not Found', 'thirty_lines_properties' ),
		'no_terms'                   => __( 'No items', 'thirty_lines_properties' ),
		'items_list'                 => __( 'Items list', 'thirty_lines_properties' ),
		'items_list_navigation'      => __( 'Items list navigation', 'thirty_lines_properties' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'team_branch', array( 'team' ), $args );

}
add_action( 'init', 'team_tax', 0 );