<?php
/**
 * Custom Post Types
 */


// Register Properties Custom Post Type
function prop_post_type() {

	$labels = array(
		'name'                  => _x( 'Properties', 'Post Type General Name', 'thirty_lines_properties' ),
		'singular_name'         => _x( 'Property', 'Post Type Singular Name', 'thirty_lines_properties' ),
		'menu_name'             => __( 'Properties', 'thirty_lines_properties' ),
		'name_admin_bar'        => __( 'Properties', 'thirty_lines_properties' ),
		'archives'              => __( 'Item Properties', 'thirty_lines_properties' ),
		'parent_item_colon'     => __( 'Parent Property:', 'thirty_lines_properties' ),
		'all_items'             => __( 'All Properties', 'thirty_lines_properties' ),
		'add_new_item'          => __( 'Add New Property', 'thirty_lines_properties' ),
		'add_new'               => __( 'Add Property', 'thirty_lines_properties' ),
		'new_item'              => __( 'New Property', 'thirty_lines_properties' ),
		'edit_item'             => __( 'Edit Property', 'thirty_lines_properties' ),
		'update_item'           => __( 'Update Property', 'thirty_lines_properties' ),
		'view_item'             => __( 'View Property', 'thirty_lines_properties' ),
		'search_items'          => __( 'Search Properties', 'thirty_lines_properties' ),
		'not_found'             => __( 'Not found', 'thirty_lines_properties' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'thirty_lines_properties' ),
		'featured_image'        => __( 'Featured Image', 'thirty_lines_properties' ),
		'set_featured_image'    => __( 'Set featured image', 'thirty_lines_properties' ),
		'remove_featured_image' => __( 'Remove featured image', 'thirty_lines_properties' ),
		'use_featured_image'    => __( 'Use as featured image', 'thirty_lines_properties' ),
		'insert_into_item'      => __( 'Insert into item', 'thirty_lines_properties' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'thirty_lines_properties' ),
		'items_list'            => __( 'Items list', 'thirty_lines_properties' ),
		'items_list_navigation' => __( 'Items list navigation', 'thirty_lines_properties' ),
		'filter_items_list'     => __( 'Filter items list', 'thirty_lines_properties' ),
	);
	$args = array(
		'label'                 => __( 'Property', 'thirty_lines_properties' ),
		'description'           => __( 'Properties', 'thirty_lines_properties' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 7,
		'menu_icon'             => 'dashicons-admin-multisite',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'properties', $args );

}
add_action( 'init', 'prop_post_type', 0 );



// Register Floorplans Custom Post Type
function floorplan_post_type() {

	$labels = array(
		'name'                  => _x( 'Floorplans', 'Post Type General Name', 'thirty_lines_properties' ),
		'singular_name'         => _x( 'Floorplan', 'Post Type Singular Name', 'thirty_lines_properties' ),
		'menu_name'             => __( 'Floorplans', 'thirty_lines_properties' ),
		'name_admin_bar'        => __( 'Floorplans', 'thirty_lines_properties' ),
		'archives'              => __( 'Floorplan Archives', 'thirty_lines_properties' ),
		'parent_item_colon'     => __( 'Parent Floorplan:', 'thirty_lines_properties' ),
		'all_items'             => __( 'All Floorplans', 'thirty_lines_properties' ),
		'add_new_item'          => __( 'Add New Floorplan', 'thirty_lines_properties' ),
		'add_new'               => __( 'Add Floorplan', 'thirty_lines_properties' ),
		'new_item'              => __( 'New Floorplan', 'thirty_lines_properties' ),
		'edit_item'             => __( 'Edit Floorplan', 'thirty_lines_properties' ),
		'update_item'           => __( 'Update Floorplan', 'thirty_lines_properties' ),
		'view_item'             => __( 'View Floorplan', 'thirty_lines_properties' ),
		'search_items'          => __( 'Search Floorplan', 'thirty_lines_properties' ),
		'not_found'             => __( 'Not found', 'thirty_lines_properties' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'thirty_lines_properties' ),
		'featured_image'        => __( 'Featured Image', 'thirty_lines_properties' ),
		'set_featured_image'    => __( 'Set featured image', 'thirty_lines_properties' ),
		'remove_featured_image' => __( 'Remove featured image', 'thirty_lines_properties' ),
		'use_featured_image'    => __( 'Use as featured image', 'thirty_lines_properties' ),
		'insert_into_item'      => __( 'Insert into item', 'thirty_lines_properties' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'thirty_lines_properties' ),
		'items_list'            => __( 'Items list', 'thirty_lines_properties' ),
		'items_list_navigation' => __( 'Items list navigation', 'thirty_lines_properties' ),
		'filter_items_list'     => __( 'Filter items list', 'thirty_lines_properties' ),
	);
	$args = array(
		'label'                 => __( 'Floorplan', 'thirty_lines_properties' ),
		'description'           => __( 'Floorplans', 'thirty_lines_properties' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-layout',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'floorplans', $args );

}
add_action( 'init', 'floorplan_post_type', 0 );