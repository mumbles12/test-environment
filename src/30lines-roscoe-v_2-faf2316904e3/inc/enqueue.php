<?php

/**
 * Enqueue scripts and styles.
 */
function thirty_lines_properties_scripts() {
	wp_enqueue_script( 'jquery' );			

	wp_enqueue_style( 'thirty_lines_properties-style', get_stylesheet_uri() );

	wp_enqueue_style( 'thirty_lines_fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );

	wp_enqueue_style('thirty_lines_properties_font', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700,700i|Rozha+One');

	wp_enqueue_script('angular-touch', "http://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-touch.js", array('angular-min'), null, false);			
	
	wp_enqueue_script( 'angular-foundation', get_template_directory_uri() . '/js/angular-stuff/angular-foundation.min.js', array('angular-min'), null, false );

	wp_enqueue_script( 'thirty_lines_properties-navigation', get_template_directory_uri() . '/build/js/app.js', array(), null, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'thirty_lines_properties_scripts' );		