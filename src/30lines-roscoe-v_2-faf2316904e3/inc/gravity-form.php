<?php
if(has_action('gravityform_activate')) {
// this function disables GF CSS on theme activation
function gravity_css_off_theme_activate() {
	update_option( 'rg_gforms_disable_css', '1' );
	update_option( 'rg_gforms_enable_html5', '1' );
}
add_action('after_switch_theme','gravity_css_off_theme_activate');

// this functiona disables GC CSS on the GF plugin activation
function gravityform_activate() {
	update_option( 'rg_gforms_disable_css', '1' );
	update_option( 'rg_gforms_enable_html5', '1' );
}
register_activation_hook(__FILE__,'gravityform_activate');
}