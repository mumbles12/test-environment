<?php global $post; ?>
<section id="gridContainer" class="grid-results is-active is-grid-v is-left-sidebar">

	<?php if ( have_rows('image_links') ) : while ( have_rows('image_links') ) : the_row();
		$featuredPage = get_sub_field('page');
		$background = wp_get_attachment_image_src( get_post_thumbnail_id( $featuredPage->ID ), 'full' ); ?>
		<div class="is-pre-footer-cta">
			<a href="<?php echo esc_url($featuredPage->guid); ?>">
				<figure class="is-photo">
					<img src="<?php echo $background[0]; ?>" alt="<?php echo esc_html($featuredPage->post_title); ?>">
				</figure>
				<section class="is-details">
					<div class="details-wrapper">
						<h4><?php echo esc_html($featuredPage->post_title); ?></h4>
					</div>
				</section>
			</a>
		</div>
	<?php endwhile;endif;wp_reset_postdata(); ?>

</section>