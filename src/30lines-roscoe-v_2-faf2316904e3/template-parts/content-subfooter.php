<section class="cta-strip is-subfooter">
	<ul class="cta-menu clearfix">
		<li><a href="<?php echo esc_url(home_url('/')); ?>communities">Communities</a></li>
		<li><a href="<?php echo esc_url(home_url('/')); ?>about">About Roscoe</a></li>
		<li><a href="<?php echo esc_url(home_url('/')); ?>careers">Careers</a></li>
		<li><a href="<?php echo esc_url(home_url('/')); ?>contact">Contact</a></li>
	</ul>
</section>