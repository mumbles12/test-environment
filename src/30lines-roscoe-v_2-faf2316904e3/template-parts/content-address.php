<?php 
$phone = get_field('phone_number', 'option');
?>

<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
	<span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span><br/>
	<span itemprop="addressLocality"><?php the_field('city_state_zip', 'option'); ?></span><br/>
	<i class="fa fa-phone"></i> <a href="tel:<?php echo stringy($phone); ?>"><span itemprop="telephone"><?php echo $phone; ?></span></a>
</div>