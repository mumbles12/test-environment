<section class="is-map-search-container" id="mapResults" ng-controller="AdvancedSearchMapStyles">
	<ui-gmap-google-map center='ui_map.center' zoom='ui_map.zoom' options="advancedSearchMapStyles">
	    <ui-gmap-markers 
	        models="filteredProperties" 
	        idKey="'ID'" 
	        coords="'self'" 
	        icon="'icon'" 

	        doCluster="true" 
			clusterOptions="ui_map_clusterOptions" 

	        fit="true" 
	        
	        click="ui_map_marker_click"
	    >
	        <ui-gmap-windows>
	            <div class="rp-map-marker" ng-non-bindable>

					<figure class="is-photo">
						<a ng-href="{{ permalink }}">
							<img ng-src="{{ image }}" alt="">
						</a>
					</figure>

	              <h4><a ng-href="{{ permalink }}">
						<span itemprop="name">{{ _title }}</span>
					</a></h4>

					<p class="is-comm-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					    <span itemprop="streetAddress">{{ meta_data.propAddress }}</span>
					    </br>
					    <span itemprop="addressLocality">{{ meta_data.propCity }}</span>,
					   	<span itemprop="addressRegion">{{ meta_data.propState }}</span>
					   	<span itemprop="postalCode">{{ meta_data.propZip }}</span>
					</p>

					<p>
						{{ meta_data.propPhoneNumber | html_for_phone }}
						<br>
						{{ meta_data.propEmail | html_for_email }}
					</p>

					{{ availableAtEachBedLevel | html_for_availableAtEachBedLevel }}
	            </div>
	        </ui-gmap-windows>
	    </ui-gmap-markers>
	</ui-gmap-google-map>


	<style>.angular-google-map-container { display: block; height: 600px; }</style>		
</section>