<?php 
// check for rows (sub repeater)
if( have_rows('image_text_boxes') ): ?>

	<ul>

	<?php 
	// loop through rows (sub repeater)
	while( have_rows('image_text_boxes') ): the_row();

		// display each item as a list - with a class of completed ( if completed )
		?>
		<li <?php if( get_sub_field('completed') ){ echo 'class="completed"'; } ?>><?php the_sub_field('name'); ?></li>
	<?php endwhile; ?>

	</ul>
	
<?php endif; //if( get_sub_field('items') ): ?>