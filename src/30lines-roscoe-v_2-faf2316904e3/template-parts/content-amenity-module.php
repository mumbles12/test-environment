

<section class="amenity-module clearfix medium-collapse" id="amenityModule">
	
	<aside class="amenity-bullet-points">
		<div class="data-wrapper">

			<h3>Amenities</h3>
			
			<header id="amenityToggles">
				<span class="button is-active" id="communitySelect">
					Your Community
				</span>
				<span class="button" id="apartmentSelect">
					Your Apartment
				</span>
			</header>


			<section class="community-bullet-points am-points is-active">
				<h5>In Your Community</h5>

				<ul class="amenity-photo-list">
					<?php
						// check if the repeater field has rows of data
						if( have_rows('comm_amenities') ):
							$i = 0;
						 	// loop through the rows of data
						    while ( have_rows('comm_amenities') ) : the_row();
								$i++;
						        // display a sub field value
						        if( get_sub_field('photo')) {
							        echo '<li ' . 'class="has-photo" data-photo="community' . $i . '" >' . get_sub_field('bullet_point') . '</li>';
						       		} else {
						       		echo '<li>' . get_sub_field('bullet_point') . '</li>';
						       	}
						    endwhile;
						else :

						    // no rows found

						endif;
					?>
				</ul>
			</section>

			<section class="apartment-bullet-points am-points">
				<h5>In Your Apartment</h5>

				<ul class="amenity-photo-list">
					<?php
						// check if the repeater field has rows of data
						if( have_rows('apart_amenities') ):
							$i = 0;
						 	// loop through the rows of data
						    while ( have_rows('apart_amenities') ) : the_row();
								$i++;
						        // display a sub field value
						        if( get_sub_field('photo')) {
							        echo '<li ' . 'class="has-photo" data-photo="apartment' . $i . '" >' . get_sub_field('bullet_point') . '</li>';
						       		} else {
						       		echo '<li>' . get_sub_field('bullet_point') . '</li>';
						       	}
						    endwhile;
						else :

						    // no rows found

						endif;
					?>
				</ul>
			</section>

		</div>
	</aside>

	<div class="amenity-gallery" id="amenityGallery">
		<?php
			// check if the repeater field has rows of data
			if( have_rows('comm_amenities') ):
				// $photo = get_sub_field('photo');
				$i = 0;
			 	// loop through the rows of data
			    while ( have_rows('comm_amenities') ) : the_row();
					$i++;
			        // display a sub field value
			        if ( get_sub_field('photo') ) : ?>

				        <div class="has-bg-img am-img" id="community<?php echo $i; ?>" style="background-image: url(<?php the_sub_field('photo'); ?>)"></div>

			       	<?php else: ?>
			       		

			    <?php endif; endwhile;
			else :

			    // no rows found

			endif;
		?>


		<?php
			// check if the repeater field has rows of data
			if( have_rows('apart_amenities') ):
				// $photo = get_sub_field("photo");
				$i = 0;
			 	// loop through the rows of data
			    while ( have_rows('apart_amenities') ) : the_row();
					$i++;
			        // display a sub field value
			        if( get_sub_field('photo')) : ?>

				        <div class="has-bg-img am-img" id="apartment<?php echo $i; ?>" style="background-image: url(<?php the_sub_field('photo'); ?>)"></div>

			       	<?php else: ?>
			       		

			    <?php endif; endwhile;
			else :

			    // no rows found

			endif;
		?>
		<!-- <div class="amenity-img-overlay"></div> -->
	</div>

	<script type="text/javascript">
		(function($) {
			$(document).ready(function() {
				// If there are no images showing in the amenity gallery, hide the gallery column aand make sure the amenity list column
				// reaches 100% width of its container
				if ( $('.am-img').length == 0 ) {
					$('#amenityGallery').remove();
					$('aside.amenity-bullet-points').css('width', '100%');
					$('aside.amenity-bullet-points').css('max-width', '100%');
				}
			});
		})(jQuery);
	</script>

</section><!-- amenityModule -->