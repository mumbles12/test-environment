<section class="map">
			
	<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBc4fQfM_CEHN26YrvsYjPyEZXGRuYRlCQ&sensor=false&extension=.js'></script> 
	 
	<script> 
	    google.maps.event.addDomListener(window, 'load', init);
	    var map;
	    function init() {
	        var mapOptions = {
	            center: new google.maps.LatLng(32.824211,-8.935548),
	            zoom: 7,
	            zoomControl: true,
	            zoomControlOptions: {
	                style: google.maps.ZoomControlStyle.DEFAULT,
	            },
	            disableDoubleClickZoom: true,
	            mapTypeControl: true,
	            mapTypeControlOptions: {
	                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	            },
	            scaleControl: true,
	            scrollwheel: false,
	            panControl: true,
	            streetViewControl: true,
	            draggable : true,
	            overviewMapControl: true,
	            overviewMapControlOptions: {
	                opened: false,
	            },
	            mapTypeId: google.maps.MapTypeId.ROADMAP,
	            styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}],
	        }
	        var mapElement = document.getElementById('metroMap');
	        var map = new google.maps.Map(mapElement, mapOptions);
	        var locations = [
	['Sheraton', 'undefined', 'undefined', 'undefined', 'undefined', 41.8891327, -87.61946419999998, 'https://mapbuildr.com/assets/img/markers/default.png']
	        ];
	        for (i = 0; i < locations.length; i++) {
				if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
				if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
				if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
	           if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
	           if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
	            marker = new google.maps.Marker({
	                icon: markericon,
	                position: new google.maps.LatLng(locations[i][5], locations[i][6]),
	                map: map,
	                title: locations[i][0],
	                desc: description,
	                tel: telephone,
	                email: email,
	                web: web
	            });
	link = '';     }

	}
	</script>
	<style>
	    #metroMap {
	        height:400px;
	        width:100%;
	    }
	    .gm-style-iw * {
	        display: block;
	        width: 100%;
	    }
	    .gm-style-iw h4, .gm-style-iw p {
	        margin: 0;
	        padding: 0;
	    }
	    .gm-style-iw a {
	        color: #4272db;
	    }
	</style>

	<div id='metroMap'></div>

</section>