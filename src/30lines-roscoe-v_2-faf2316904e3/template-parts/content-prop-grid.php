<?php 
global $post, $rentPress_Service;
//$properties = json_decode($rentPress_Service['properties']->forGoogleMap(), true); ?>
<section id="gridContainer" class="grid-results is-active is-grid-v">
	<rent-press-properties init-query-args='{"posts_per_page": 4, "properties_from_neighborhood": <?php echo get_post_meta($post->ID, "post_type_connected_to_neighborhoods", true);?>, "post__not_in": [<?php echo $post->ID; ?>], "properties_of_similar": true}'>
		<div class="is-property" ng-repeat="property in filteredAndOrderedProperties | limitTo: 4">
			<a ng-href="{{ property.permalink }}">
				<figure class="is-photo">
					<img ng-src="{{ property.image }}" alt="{{ property._name }}">
				</figure>
				<section class="is-details">
					<div class="details-wrapper">
						<h4>{{ property._title }}</h4>
						<h6>Explore <span>Community</span></h6>
						<span class="is-phone">{{ property.meta_data.propPhoneNumber }}</span>
					</div>
				</section>
			</a>
		</div>
	</rent-press-properties>
</section>