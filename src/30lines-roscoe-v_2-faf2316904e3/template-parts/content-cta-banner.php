<?php 
global $post;
$title = 'Ready to see it <span>in person?</span>';
$buttonText = 'Schedule a Tour';
$buttonLink = '/tour';
if ( is_singular('floorplans') ) :
	$buttonLink = '/tour?'.urlencode(esc_html($post->fpName)).'&bed_count='.esc_attr($post->fpBeds);
endif; ?>

<section class="cta-tour has-bg-img">
	<div class="trans-wrapper">
		<div class="row">
			<div class="medium-6 columns cta-text">
				<h4><?php echo $title; ?></h4>
			</div>
			<aside class="medium-6 columns cta-btn">
				<a href="<?php echo esc_url($buttonLink); ?>" class="button white-ghost-btn">
					<?php echo sanitize_text_field($buttonText); ?>
				</a>
			</aside>
		</div>
	</div>
</section>