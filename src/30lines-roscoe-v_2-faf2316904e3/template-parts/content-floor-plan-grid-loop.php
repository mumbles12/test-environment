<div class="is-floorplan">
	<figure class="is-photo">
		<a href="#">
			<img src="http://placehold.it/200x150" alt="">
		</a>
	</figure>
	<section class="floorplan-data">
		<h4>A6</h4>
		<h6>From $1,349/MO.</h6>

		<p class="is-count">
			<span>1 Bed</span> <span>1 Bath</span> | <span>675 Sq. Ft.</span> 
		</p>
		<p><strong>2 Available Now</strong></p>

		<a href="#" class="button">View Details</a>
	</section>
</div> <!-- is-floorplan -->
