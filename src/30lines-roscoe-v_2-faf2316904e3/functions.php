<?php
/*============ Top Line Service -- since 2/11/2016 | 30Lines ==================*/

/**
 * Set custom ability to easily attach images to a post for access by get_children();
 */
add_filter("manage_upload_columns", 'topline_upload_columns');
add_action("manage_media_custom_column", 'topline_media_custom_columns', 0, 2);

/**
 * Set column for image parent re-attachment
 * @param array $columns // wp table columns for media uploads
 */
function topline_upload_columns($columns) {
    unset($columns['parent']);
    $columns['better_parent'] = "Parent";
    return $columns;
}

/**
 * Manage column layout for image parent re-attachment
 * @param array $column_name // Name of the column
 * @param integer $id // Post ID
 */
function topline_media_custom_columns($column_name, $id) {
    $post = get_post($id);

    if($column_name != 'better_parent')
            return;
            if ( $post->post_parent > 0 ) {
                    if ( get_post($post->post_parent) ) {
                            $title =_draft_or_post_title($post->post_parent);
                    }
                    ?>
                    <strong><a href="<?php echo get_edit_post_link( $post->post_parent ); ?>"><?php echo $title ?></a></strong>, <?php echo get_the_time(__('Y/m/d')); ?>
                    <br />
                    <a class="hide-if-no-js" onclick="findPosts.open('media[]','<?php echo $post->ID ?>');return false;" href="#the-list"><?php _e('Re-Attach'); ?></a></td>

                    <?php
            } else {
                    ?>
                    <?php _e('(Unattached)'); ?><br />
                    <a class="hide-if-no-js" onclick="findPosts.open('media[]','<?php echo $post->ID ?>');return false;" href="#the-list"><?php _e('Attach'); ?></a>
                    <?php
            }

}

/**
 * Get floor plans for a property on WP -- to be used in subfooter-ind-property.php
 * @param string $taxonomyterm
 * @return array
 */
function getFloorPlansByTerm($taxonomyterm) {
    global $rentPress_Service;
    return $rentPress_Service['floorplans']->byPropertyTaxonomy($taxonomyterm);
}

/**
 * Get all featured properties
 * @return array
 * @note Cached information lasts for an entire day before being re-initialized
 */
function getFeaturedProperties() {
    global $rentPress_Service;
    return $rentPress_Service['properties']->all();
}

/**
 * Retrieve all properties -- if there are cached results, it will use those
 * @return object The result of WP_Query for top line properties 
 * @note Cached information lasts for an entire day before being re-initialized
 */

function topline_cache_all_properties() {
    global $rentPress_Service;
    return $rentPress_Service['properties']->all();
}

/**
 * Retrieve all floor plans -- if there are cached results, it will use those
 * @return object The result of WP_Query for top line floor plans
 * @note Cached information lasts for an entire day before being re-initialized
 */

function topline_cache_all_floorplans() {
    global $rentPress_Service;
    return $rentPress_Service['floorplans']->all();
}

/** 
 * @param WP_Post_Object $property // all post meta of a property 
 * @return string
 */
function calculateRentMin($property) {
    $rentMin = number_format($property['propMinRent'][0]);
    $rentMax = number_format($property['propMaxRent'][0]);

    // If the min is zero, but the max is not, use that
    if( $rentMin == 0 && ( $rentMax > $rentMin ) ) return 'Starting at $'. (string) $rentMax;

    // If the max rent also equals zero, then return not found warning
    if ( $rentMin == 0 && $rentMax == 0 ) {
        return false;
    }
    return 'Starting at $'. (string) $rentMin;
}

/**
 * @param WP_Post_Object $property // all post meta of a property
 * @return string 
 */
function calculateMaxSQFT($property) {
    $maxSqrFt = number_format($property['propMaxSQFT'][0]);
    $minSqrFt = number_format($property['propMinSQFT'][0]);

    // If there is no max, but there is a min, display that
    if( $maxSqrFt == 0 && ($maxSqrFt < $minSqrFt)) return (string) $minSqrFt;

    // If it's a zero value, return default 
    if( $maxSqrFt == 0 ) return (string) 0;

    return (string) $maxSqrFt;
}

/**
 * @param WP_Post_Object $property // all post meta of a property
 * @return string
 */
function calculateBathRange($property) {
    $min = number_format($property['propMinBaths'][0]);
    $max = number_format($property['propMaxBaths'][0]);

    if($min > 0) $response = $min;
    
    if(($max == $min) && $max == 0) {
        $response = '[Not listed]';
    } elseif ( $max > $min ) {
        $response .= '- '.$max;
    } else {
        $response .= '- '.$max;
    }

    return (string) $response;

}

/** 
 * @param WP_Post_Object $property // all post meta of a property
 * @return string
 */
function calculateBedRange($property) {
    $min = number_format($property['propMinBeds'][0]);
    $max = number_format($property['propMaxBeds'][0]);
    if($min == 0) {
        $response = 'Studio ';
    } else {
        $response = $min.' ';
    }
    
    if(($max == $min) && $min == 0) {
        $response = '[Not listed]';
    } elseif ( $max > $min ) {
        $response .= '- '.$max;
    } else {
        $response .= '- '.$max;
    }

    return (string) $response;
}

/**
 * Check if value to check is equal to the value received for cross-check
 * @param mixed $valueToCheck 
 * @param mixed $valueReceived 
 * @return BOOL
 */
function topline_isSelected($valueToCheck, $valueReceived) {
    if ( $valueToCheck === $valueReceived ) return true;
    return false;
}

/**
 * Fetches list of low tier rent from all properties on site
 * @param array $floorplans
 * @return int
 */
function propertyRentMin($floorplans = null) {
    global $rentPress_Service;
    $floorplans = ! isset($floorplans) ? $rentPress_Service['floorplans']->all() : $floorplans;
    return $rentPress_Service['properties']->incrementalLowTierRent(500, 250, 2000, $floorplans);
}

/**
 * @param $floorplans
 * @return int
 */
function propertyRentMax($floorplans = null) {
    global $rentPress_Service;
    $floorplans = ! isset($floorplans) ? $rentPress_Service['floorplans']->all() : $floorplans;
    return $rentPress_Service['properties']->incrementalHighTierRent(2000, 250, 4000, $floorplans);
}

/**
 * @param $floorplans
 * @return string
 */
function propertyRentRange( $floorplans = null ) {
    global $rentPress_Service;
    return $rentPress_Service['properties']->rentPressRentRange( $floorplans );
}

/**
 * @param $floorplans
 * @param bool $studio
 * @return int|string
 */
function propertyMaxBeds( $floorplans = null ) {
    global $rentPress_Service;
    return $rentPress_Service['properties']->incrementalBedRange( $floorplans );
}

/**
 * Gets list of all bath counts available -- used for search filter or querying 
 * @param  [optional] array $floorplans // List of all available floor plans
 * @return array // list of bath counts
 */
function propertyMaxBaths( $floorplans = null ) {
    global $rentPress_Service;
    return $rentPress_Service['properties']->incrementalBathRange( $floorplans );
}

/**
 *  Lead integration for entrata
 */

/**
 * Submit lead to RentCafe 
 *
 * @param array $entries // entries provided by the requesting user
 * @return Response
 */
function top_line_reserve_lead($entries){
    include_once( TOP_LINE_PLUGIN_DIR . 'leads/topline_LeadGeneration.php' );
    // provide username and password provided by client RentCafe account to our base class
    $topLineLeads = new topline_LeadGeneration('mike@30lines.com', 'roscoe123');
    $message = preg_replace('/ /', '%20', $entries['message']);
    $phone = preg_replace( '/[\(\)\s\-]/', '', $entries['phone']);
    $data = [
        'email' => $entries['email'],
        'message' => $message,
        'propCode' => $entries['propCode'],
        'phone' => $phone,
        'moveIn' => $entries['moveInDate']
    ];
    return $topLineLeads->topline_submit_contact_reserve_lead($data);
}

/**
 * Cut the excerpt to a defined length 
 * @param integer $charlength // desired character length of string 
 * @return string 
 */
function the_excerpt_max_charlength($charlength, $excerpt = null, $readMoreLink = null) {
    $excerpt = isset($excerpt) ? $excerpt : get_the_excerpt();
    $charlength++;

    if ( mb_strlen( $excerpt ) > $charlength ) {
        $subex = mb_substr( $excerpt, 0, $charlength - 5 );
        $exwords = explode( ' ', $subex );
        $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
        if ( $excut < 0 ) {
                echo mb_substr( $subex, 0, $excut );
        } else {
                echo $subex;
        }
        echo isset($readMoreLink) ? '<br/><a class="readmore-btn" href="'.$readMoreLink.'" style="color: #75A032;">... Read More</a>' : '[...]';
    } else {
        echo $excerpt;
    }
}

/*============ END TOP LINE ADDITIONS -- since 2/5/2016 | 30Lines ================================*/

/**
 * 30 Lines Properties functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package 30_Lines_Properties
 */

if ( ! function_exists( 'thirty_lines_properties_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function thirty_lines_properties_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on 30 Lines Properties, use a find and replace
	 * to change 'thirty_lines_properties' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'thirty_lines_properties', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	// RV1
	// add_image_size( 'slider', 300, 300, true );
	// add_image_size( 'community', 750, 500,true );
	// add_image_size( 'slider-large', 1000, 550,true );

	// add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );
	// function custom_image_sizes_choose( $sizes ) {
	//   $custom_sizes = array(
	//       'community' => 'Medium Plus'
	//   );
	//   return array_merge( $sizes, $custom_sizes );
	// }

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'thirty_lines_properties' ),
		'footer' => esc_html__( 'Footer Menu', 'thirty_lines_properties' ),
		'social_links' => esc_html__( 'Social', 'thirty_lines_properties' ),
		'living' => __( 'Living With Roscoe' ),
	    'right_area' => __( 'The Right Area' ),
	    'right_size' => __( 'The Right Size' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );


	// register_sidebar( array(
	// 	'name'          => __( 'Sidebar', 'thirty_lines_properties' ),
	// 	'id'            => 'sidebar-1',
	// 	'description'   => '',
	// 	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	// 	'after_widget'  => '</aside>',
	// 	'before_title'  => '<h2 class="widget-title">',
	// 	'after_title'   => '</h2>',
	// ) );

	register_sidebar( array(
		'name'          => __( 'Right Area', 'thirty_lines_properties' ),
		'id'            => 'right-area',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'thirty_lines_properties_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'thirty_lines_properties_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function thirty_lines_properties_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'thirty_lines_properties_content_width', 640 );
}
add_action( 'after_setup_theme', 'thirty_lines_properties_content_width', 0 );

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Enqueue scripts and styles
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Enqueue business logos and address
 */
require get_template_directory() . '/inc/business-info.php';

/**
 * Clean up Gravity Forms styles
 */
// require get_template_directory() . '/inc/gravity-forms.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Register Custom Post Types
 */
require get_template_directory() . '/inc/leadership.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

require get_template_directory() . '/inc/neighborhoods.php';

if ( isset($_GET['update_roscoe_amenities']) ) {
    /** Temporary Amenity Action - Only fires when specific query var present */
    require get_template_directory() . '/inc/rentpress-temp-amenity-import.php';
}

add_filter('query_vars', function($vars) {
    $vars[]='anywhere';

    return $vars;
});

add_action('pre_get_posts', function($query) {
    if ($query->get('post_type') == 'properties' && ! $query->is_singular) {
        if (rentPress_searchHelpers::if_query_var_check($query->get('anywhere'))) {

            $rpOptions=new rentPress_Options();

            $names_to_short=array_flip(rentPress_searchHelpers::$states);

            $state_shortname=$names_to_short[ ucwords( $query->get('anywhere') ) ];
            $state_longname=rentPress_searchHelpers::$states[ strtoupper($query->get('anywhere')) ];

            if (! is_null($state_shortname) && strlen($query->get('anywhere')) > 2) {
                $maybeStates=[];

                foreach ($names_to_short as $longName => $shortName) {
                    if (strpos($longName, ucwords($query->get('anywhere'))) !== false) {
                        $maybeStates[]=$shortName;
                    }
                } 
            }

            $properties_meta_query=[];

            $properties_meta_query['anywhere']=[
                'relation' => 'OR',
                
                'propName' => [
                    'key' => 'propName',
                    'compare' => 'LIKE',
                    'value' => $query->get('anywhere'),
                ],

                'PropTerms' => [
                    'key' => 'property_searchterms',
                    'compare' => 'LIKE',
                    'value' => $query->get('anywhere'),
                ],

                'propAddress' => [
                    'key' => 'propAddress',
                    'compare' => 'EXISTS', 
                ],
            ];

            if (! is_null($state_shortname) || ! is_null($state_longname) || (isset($maybeStates) || count($maybeStates) > 0)) {
                if (! is_null($state_shortname)) {
                    $properties_meta_query['anywhere']['state']=[
                        'key' => 'propState',
                        'value' => $state_shortname
                    ];
                }

                if (! is_null($state_longname)) {
                    $properties_meta_query['anywhere']['state']=[
                        'key' => 'propState',
                        'value' => strtoupper($query->get('anywhere'))
                    ];                                       
                }

                if (isset($maybeStates) && count($maybeStates) > 0) {
                    $properties_meta_query['anywhere']["state"]=[
                        'key' => 'propState',
                        'compare' => 'IN',
                        'value' => $maybeStates,
                    ];
                }
            }
            else {
                $ch = curl_init();

                $something=$query->get('anywhere');

                $url="https://maps.googleapis.com/maps/api/geocode/json?address=". urlencode($something) ."&sensor=false&key=". $rpOptions->getOption('google_api_token');
                
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                
                curl_setopt($ch, CURLOPT_URL, $url);

                $geoloc = json_decode(curl_exec($ch), true);

                $step1 = $geoloc['results'];
                $step2 = $step1[0]['geometry'];
                $coords = $step2['location'];

                $query->set('properties_within_distance_from', [
                    'lat' => $coords['lat'],
                    'lng' => $coords['lng'],
                ]);

                curl_close($ch);
            }

            if (count($properties_meta_query) >= 1) {
                $currentMetaQuery=$query->get('meta_query');
                    if (is_array($currentMetaQuery)) {
                        $currentMetaQuery[]=$properties_meta_query;
                    }
                    else {
                        $currentMetaQuery=[ 'relation' => 'OR', $properties_meta_query ];
                    }

                    $query->set('meta_query', $currentMetaQuery);
            }
            
        }
    }
}, 9);

add_filter("rentPressAngular_filterValues", function($filter_values) {
    global $wpdb;

    foreach ($filter_values['neighborhoods'] as $neighborhood) {
        $states=wp_get_post_terms($neighborhood->ID, 'state', ['fields' => 'names']);

        $neighborhood->state=$states[0];
    }

    foreach ($filter_values['properties'] as $property) {
        $property->city=get_post_meta($property->ID, 'propCity', true);
        $property->state=get_post_meta($property->ID, 'propState', true);

        $property->searchTerms=get_post_meta($property->ID, 'property_searchterms', true);
    }

    $filter_values['zipcodes_with_cities_and_states']=$wpdb->get_results("
        SELECT DISTINCT pm1.meta_value as zip, pm3.meta_value as city, pm2.meta_value as state, 
        CONCAT(pm1.meta_value, ', ', pm2.meta_value) as zipState, CONCAT(pm2.meta_value, ', ', pm1.meta_value) as stateZip, CONCAT(pm3.meta_value,', ', pm2.meta_value, ', ', pm1.meta_value) as cityStateZip
        FROM $wpdb->posts p
        INNER JOIN $wpdb->postmeta pm1 ON p.ID = pm1.post_id
        INNER JOIN $wpdb->postmeta pm2 ON p.ID = pm2.post_id
        INNER JOIN $wpdb->postmeta pm3 ON p.ID = pm3.post_id
        WHERE p.post_type = 'properties' AND pm1.meta_value != '' AND pm1.meta_key = 'propZip' AND pm2.meta_key = 'propState' AND pm3.meta_key = 'propCity'
    "); 

    return $filter_values;
});

add_filter('RoscoePropertyAmenities', function( $propCommunityAmenities, $propAmenities, $pets, $tags, $commAmenities, $aptAmenities ) {
    global $post;
    $html="";
    $amenityTitleToClassName = [];
    $featuredAmenities = get_field('featured_amenities', $post->ID);

    $amenityTitleToClassName = [
        'Pool'           => 'pool',
        'Fitness Center' => 'fitness-center'
    ];

    if ( isset($aptAmenities) || isset($commAmenities) ) {
        $featuredAmenityList = [];
        $aptFeatured = array_filter(array_merge($aptAmenities, $commAmenities), function($amenity, $key) {
            return strpos($amenity['bullet_point'], '*') !== false ? true : false;
        }, ARRAY_FILTER_USE_BOTH);
        foreach ($aptFeatured as $amenity) {
            $featuredAmenityList[$amenity['bullet_point']] = sanitize_title($amenity['bullet_point']);
        }
        $amenityTitleToClassName = array_merge($amenityTitleToClassName, $featuredAmenityList);
    }

    // if ( $featuredAmenities ) { // Add more icons from featured amenities ACF
    //     $additionalAmenityClasses = [];
    //     array_walk($featuredAmenities, function(&$amenity, $amenityKey) use ( &$additionalAmenityClasses ) {
    //         $additionalAmenityClasses[$amenity['amenity_name']] = strlen($amenity['icon']) ? " fa {$amenity['icon']}" : sanitize_title($amenity['amenity_name']);
    //     });
    //     $amenityTitleToClassName = array_merge($additionalAmenityClasses, $amenityTitleToClassName);
    // }

    $amenitiesDisplayed = [];

    if (isset($featuredAmenities)) {
        $amenitiesDisplayed = [];
        foreach ($featuredAmenities as $amenity) {
            foreach ($amenityTitleToClassName as $title => $classname) {
                $classname = strlen($amenity['icon']) ? " fa {$amenity['icon']}" : sanitize_title($amenity['amenity_name']);
                if ( 
                    strpos($amenity['amenity_name'], $title) !== false &&  // If is in list of supported feat. amenities
                    ! in_array($classname, $amenitiesDisplayed) &&  // If is not already displayed
                    strpos($amenity['amenity_name'], '*') !== false // If is considered featured by presence of '*' character
                ) {
                    $html.="<li class='". $classname ."' title='". $amenity['amenity_name']."'></li>";
                    $amenitiesDisplayed[] = $classname; 
                }
            }
        }
    }

    if (isset($aptAmenities)) {
        $amenitiesDisplayed = [];
        foreach ($aptAmenities as $amenity) {
            foreach ($amenityTitleToClassName as $title => $classname) {
                if ( 
                    strpos($amenity['bullet_point'], $title) !== false &&  // If is in list of supported feat. amenities
                    ! in_array($classname, $amenitiesDisplayed) &&  // If is not already displayed
                    strpos($amenity['bullet_point'], '*') !== false // If is considered featured by presence of '*' character
                ) {
                    $html.="<li class='". $classname ."' title='". $amenity['bullet_point']."'></li>";
                    $amenitiesDisplayed[] = $classname;
                }
            }
        }
    }

    if (isset($commAmenities)) {
        $amenitiesDisplayed = [];
        foreach ($commAmenities as $amenity) {
            foreach ($amenityTitleToClassName as $title => $classname) {
                if ( 
                    strpos($amenity['bullet_point'], $title) !== false &&  // If is in list of supported feat. amenities
                    ! in_array($classname, $amenitiesDisplayed) &&  // If is not already displayed
                    strpos($amenity['bullet_point'], '*') !== false // If is considered featured by presence of '*' character
                ) {
                    $html.="<li class='". $classname ."' title='". $amenity['bullet_point']."'></li>";
                    $amenitiesDisplayed[] = $classname;
                }
            }
        }
    }

    // if (isset($propCommunityAmenities)) {
    //     foreach ($propCommunityAmenities as $amenity) {
    //         foreach ($amenityTitleToClassName as $title => $classname) {
    //             if ( strpos($amenity->Title, $title) !== false ) {
    //                 $html.="<li class='". $classname ."' title='". $amenity->Title ."'></li>";
    //             }
    //         }
    //     }
    // }

    // if (isset($propAmenities)) {
    //     foreach ($propAmenities as $amenity) {
    //         foreach ($amenityTitleToClassName as $title => $classname) {
    //             if ( strpos($amenity->Title, $title) !== false ) {
    //                 $html.="<li class='". $classname ."' title='". $amenity->Title ."'></li>";
    //             }
    //         }
    //     }
    // }

    if (isset($pets) && count($pets) > 0) {
        $html.="<li class='pets' title='Pets!'></li>";
    }

    return $html;
}, 10, 6);

add_filter('rentPressPropertyDataForMvc', function($property) {

    $property->featured_amenities=apply_filters('RoscoePropertyAmenities', 
        $property->meta_data['propCommunityAmenities'], 
        $property->meta_data['amenities'], 
        wp_get_post_terms($property->ID, 'prop_pet_restrictions', ['fields' => 'names'])
    );

 
    $property->has_special = false;
    if(strtotime(get_field('start_date', $property->ID)) <= strtotime(date('Y-m-d')) && 
       strtotime(get_field('end_date', $property->ID)) >= strtotime(date('Y-m-d'))) {
        $property->has_special = true;
        $property->special_title = get_field('title_of_special', $property->ID);
        $property->special_description = get_field('description_of_special', $property->ID);
    }

    return $property;
});

function test_create_hoods_from_props() {
    if ( $_GET['post_type'] == 'neighborhoods' || $_GET['post_type'] == 'properties' ) {
    	global $wpdb;

        $props = new WP_Query([
            'post_type' => 'properties',
            'nopaging' => true
        ]); 
        if ( $props->have_posts() ) : while ( $props->have_posts() ) : $props->the_post();
            // Variables used to create information for post
            $postTitle = sanitize_text_field(get_post_meta($props->post->ID, 'propCity', true));

            if ($postTitle != '') {
            	$theNeighborhood=$wpdb->get_row($wpdb->prepare(
            		"
            			SELECT ID 
            			FROM $wpdb->posts
            			WHERE post_type = %s AND post_title = %s
            			LIMIT 1
            		",
            		'neighborhoods',
            		$postTitle
            	))->ID;

	            if (is_null($theNeighborhood)) {
	                $postVars = array(
	                    'post_title'      => $postTitle,
	                    'post_type'       => 'neighborhoods',
	                    'post_status'     => 'publish'
	                );

	                // Store post, store id
	                $hoodPostID = wp_insert_post($postVars);
	            
	                update_post_meta($props->post->ID, 'post_type_connected_to_neighborhoods', $hoodPostID);
	                wp_set_object_terms( $hoodPostID, get_post_meta($props->post->ID, 'propState', true), 'state', false);
	            }
	            else {
	                update_post_meta($props->post->ID, 'post_type_connected_to_neighborhoods', $theNeighborhood);
	            }
            	
            }
            
            

        endwhile;endif;wp_reset_postdata();
    }
}

add_action( 'admin_init', 'test_create_hoods_from_props' );

function property_search_vars($vars) {
    $vars[]="properties_in_city_or_zip_or_state";

    return $vars;
}

add_filter('query_vars', 'property_search_vars');
