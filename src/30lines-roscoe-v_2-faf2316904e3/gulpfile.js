var options = {};
  options.user = 'markszymanski';
  options.port = 8888;
  options.site_path = 'http://localhost:8888/30Lines_Properties/'; // something like /Users/username/sites/mymampsite

var gulp    = require('gulp');
var $       = require('gulp-load-plugins')(),
mamp        = require('gulp-mamp'),
uglify      = require('gulp-uglify'),
browserSync = require('browser-sync').create(),
concat      = require('gulp-concat');
sass        = require('gulp-sass');
sourcemaps  = require('gulp-sourcemaps');


gulp.task('config', function(cb){
    mamp(options, 'config', cb);
});

gulp.task('start', function(cb){
    mamp(options, 'start', cb);
});

gulp.task('stop', function(cb){
    mamp(options, 'stop', cb);
});

gulp.task('mamp', ['config', 'start']);


var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src('scss/style.scss')
      .pipe(sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed'
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
      .pipe(sourcemaps.write())
    .pipe(gulp.dest(''))
    .pipe(browserSync.stream());
});


// Scripts task: Uglify | Concatenate
gulp.task('scripts', function(){
  gulp.src([
    'bower_components/jquery/dist/jquery.js', 
    'bower_components/what-input/what-input.js', 
    'bower_components/foundation-sites/dist/foundation.js',
    'js/vendor/*.js',
    'js/app.js'
    ])
  .pipe(concat('app.js'))
  .pipe(uglify())
  .pipe(gulp.dest('build/js'));
});

// Watch JS
gulp.task('watch', ['sass'], function(){

  browserSync.init({
        // server: {
        //     baseDir: "./"
        // }
        proxy: 'http://localhost:8888/'
    });

  gulp.watch('js/*.js', ['scripts']);
  gulp.watch('scss/**/*.scss', ['sass']);
  gulp.watch("*.php").on('change', browserSync.reload);
});

gulp.task('basic', ['sass','scripts'], function(){
  gulp.watch('js/*.js', ['scripts']);
  gulp.watch('scss/**/*.scss', ['sass']);
});

gulp.task('default', ['sass','scripts', 'watch'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
});
