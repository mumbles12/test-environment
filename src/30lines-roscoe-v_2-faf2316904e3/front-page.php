<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 30_Lines_Properties
 */

get_header(); 
$background = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>

	<header class="home-page-hero hero has-bg-img parallax-window" data-parallax="scroll" data-image-src="<?php echo $background[0]; ?>">
		
		<h1 class="screen-reader-text"><?php the_title(); ?></h1>

	</header>

	<main id="main" role="main">
		<section class="entry-content">
			<?php
			if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php if( have_rows('pick_up_lines') ): ?>
						<!-- Pick up lines -->
						<div class="fp-pick-up-lines">
							<?php while ( have_rows('pick_up_lines') ) : the_row(); ?>
						    	<div class="a-pick-up-line">
									<a href="<?php echo get_sub_field('link'); ?>">
						    		<?php the_sub_field('the_line'); ?>
					    			</a>
					    		</div>
						    <?php endwhile; ?>
						</div>
					<?php else : ?>

					<?php endif;?>

					<?php 
						$heading_tag_line=get_field('heading_tag_line');
						if ($heading_tag_line != '') : 
					?>				
						<!-- A Tag Line -->

						<div class="fp-heading-tag-line with-bottom-accent"><h2><?php echo $heading_tag_line; ?></h2></div>

					<?php endif; ?>

					<?php if ( get_field('featured_video') ) : ?>

						<!-- Featured video -->
						<div class="fp-featured-video">
							<?php the_field('featured_video'); ?>
						</div>

					<?php endif; ?>

					<?php if( have_rows('feature_properties') && ! get_field('featured_video') ): ?>

						<!-- Featured Properties -->
						<div class="slick fp-featured-properties">
							
							<?php 
								
							while ( have_rows('feature_properties') ) : the_row();
						        $property=get_sub_field('property');
					        	$image=null;
						        $prop_replacement_image=get_sub_field('replacement_image'); 

						        if (isset($prop_replacement_image['url'])) {
						        	$image=$prop_replacement_image['url'];
						        }

					        	$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($property->ID), "full"); 
								
								if (isset($featured_image[0]) && is_null($image)) {
									$image=$featured_image[0];
								}
					        	
					        	$propertyGeneralPhotos=json_decode(get_post_meta($property->ID, 'propGeneralPhotos', true));

					        	if (isset($propertyGeneralPhotos[0]) && is_null($image)) {
					        		$image=$propertyGeneralPhotos[0]->Url;
					        	}

					        	if (is_null($image)) {
					        		$image='http://placehold.it/1000X700&text=Property Name';
					        	}

								?>

						        <div class="slide has-bg-img" style="background-image: url('<?php echo $image; ?>');">
						        	<a href="<?php echo get_permalink($property->ID); ?>">

						        		<h3 class="is-prop-title"><?php echo esc_html($property->propName); ?></h3>

						        	</a>
						        </div>

							<?php endwhile; ?>
						</div>
					<?php endif; ?>

				<?php endwhile;
			endif; ?>
		</section>

	</main>

<?php
get_footer();
